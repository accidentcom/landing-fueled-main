function pleaseWait(){
    var options = {
        theme: "custom",
        // If theme == "custom" , the content option will be available to customize the logo
        content: '<img style="width:80px;" src="' + loaderGif + '" class="center-block">',
        message: '',
        backgroundColor: "#ffffff",
        textColor: "grey"
    };
HoldOn.open(options);
}

function stopWaiting(){
    HoldOn.close();
}