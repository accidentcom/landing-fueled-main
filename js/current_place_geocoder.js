$(function () {
    var geocoder;
    var location;

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    //Get the latitude and the longitude;
    function successFunction(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        initialize();
        codeLatLng(lat, lng);
    }

    function errorFunction() {
        //alert("Geocoder failed");
    }

    function initialize() {
        geocoder = new google.maps.Geocoder();
    }

    function codeLatLng(lat, lng) {
        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({
            'latLng': latlng
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    $.each(results,function(placeKey, place){
                        $.each(place.address_components,function(componentKey, component){
                            $.each(component.types,function(componentTypeKey, componentType){
                                if(componentType == 'postal_code'){
                                    location = component.long_name;
                                    $("input.geo-complete").geocomplete("find", location);
                                    //break;
                                }
                            });
                            
                        });
                    });
                    
                } else {
                    location = null;
                }
            } else {
                location = null;
            }
        });
    }


    location = getParameterByName('l');
    if (getParameterByName('l') == null) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
        }
    }

});