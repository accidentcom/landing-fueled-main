
$(document).ready(function () {
$(".phone_number").mask("(999) 999-9999");

    // Preselect case type if passed from url
    case_type = case_type.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });

    $( window ).on( "load", function() {
        $("input[name=accident_type][value='" + case_type + "']").prop('checked', true);
        if ($('select[name=accidentType]').length > 0) {
            $("input[name=accident_type][value='" + case_type + "']").prop('checked', true);
            $('select[name=accidentType]').val(case_type);
            $('.wide').niceSelect('update');
            $('#casaType').val(case_type);
        }
    });

 
    // Test mode (true/false)
    if (test_mode == 'true') {
        $('#testMode').val('true');
    } else {
        $('#testMode').val('false');
    }

    $("#submitv2").click(function(event){
        var lT = 0;
       
        $(".address-info > input").each(function() {
            lT += $(this).val().length;
        });
        if(lT == 0){
           $("#address").val('');
           //event.preventDefault();
           return;
        }else{
            $(this).closest('form').submit();
        }
        
    });

    $("#address").keydown(function(){
        $(".address-info > input").each(function() {
             $(this).val('');
        });
    });


    $("#v2Sub").click(function(event){
        var lT = 0;
       
        $(".address-info > input").each(function() {
            lT += $(this).val().length;
        });
        if(lT == 0){
           $("#hero-form-location").val('');
           //event.preventDefault();
           return;
        }else{
            $(this).closest('form').submit();
        }
        
    });

    $("#hero-form-location").keydown(function(){
        $(".address-info > input").each(function() {
             $(this).val('');
        });
    });


    $('.popover_info').click(function(){
        $(window).scrollTop(0);
        $('#accident_auto_wizardv2').popover({placement:'top'}).popover('show');
        Focusable.setFocus($('#accident_auto_wizardv2'), {hideOnClick: true});
        setTimeout(function(){  
            $('#accident_auto_wizardv2').popover('hide');
            $('#accident_auto_wizardv2').popover('dispose');
            Focusable.hide();
        },3000);

    });

    $('.popover_info_v3').click(function(){
        $(window).scrollTop(0);
        $('#accident_auto_wizard').popover({placement:'top'}).popover('show');
        Focusable.setFocus($('#accident_auto_wizard'), {hideOnClick: true});
        setTimeout(function(){  
            $('#accident_auto_wizard').popover('hide');
            $('#accident_auto_wizard').popover('dispose');
            Focusable.hide();
        },3000);

    });

    /* SET COOKIE for main landing page */
    var siteLandingPath = window.location.href;
    if (!Cookies.get('siteLandingPath')) {
        Cookies.set('siteLandingPath', siteLandingPath);
    }
    /**/
    /* FOCUS injury box if cookie found */
    if (Cookies.get('siteLandingPath') && Cookies.get('landOnMainFormOtherPage')) {
        Cookies.remove('landOnMainFormOtherPage');
        $(window).scrollTop(0);
        $('.start-eval,#accident_auto_wizardv2,#accident_auto_wizard').popover({placement: 'top'}).popover('show');
        Focusable.setFocus($('.start-eval,#accident_auto_wizardv2,#accident_auto_wizard'), {hideOnClick: true});
        setTimeout(function () {
            $('.start-eval, #accident_auto_wizardv2,#accident_auto_wizard').popover('hide');
            $('.start-eval,#accident_auto_wizardv2,#accident_auto_wizard').popover('dispose');
            Focusable.hide();
        }, 3000);
    }
    /**/

    $('.popover_info_v2').click(function(){
        /* REDIRECT if cookie found */
        var siteLandingPath = Cookies.get('siteLandingPath');
        if (siteLandingPath) {
            var currentUrl = window.location.href;
            if (currentUrl != siteLandingPath) {
                Cookies.set('landOnMainFormOtherPage', 'true');
                window.location.assign(Cookies.get('siteLandingPath'));
            }
        }
        $(window).scrollTop(0);
        $('.start-eval,#accident_auto_wizardv2,#accident_auto_wizard').popover({placement:'top'}).popover('show');
        Focusable.setFocus($('.start-eval,#accident_auto_wizardv2,#accident_auto_wizard'), {hideOnClick: true});
        setTimeout(function(){  
            $('.start-eval, #accident_auto_wizardv2,#accident_auto_wizard').popover('hide');
            $('.start-eval,#accident_auto_wizardv2,#accident_auto_wizard').popover('dispose');
            Focusable.hide();
        },3000);

    });

    $('#mainHeroForm').validate({
        ignore: [],
        rules: {
            location: {
                required: true
            },
            accidentType: {
                required: true
            }
        },
        errorPlacement: function (error, element) {
            if (element.is('select:hidden')) {
                error.insertAfter(element.next('.nice-select'));
            } else {
                error.insertAfter(element);
            }
        },
        messages: {
            location: {
                required: "Please Enter 5 Digit Zip Code"
            },
            accidentType: {
                required: 'Select Accident Type'
            }
        }
    });


    $('#v2HeroForm').validate({
        ignore: [],
        rules: {
            location: {
                required: true
            },
            accidentType: {
                required: true
            }
        },
        errorPlacement: function (error, element) {
            if (element.is('select:hidden')) {
                error.insertAfter(element.next('.nice-select'));
            } else {
                error.insertAfter(element);
            }
        },
        messages: {
            location: {
                required: "Please Enter 5 Digit Zip Code"
            },
            accidentType: {
                required: 'Select Accident Type'
            }
        }
    });

    
$('.wide').val($(".wide option:first").val()).niceSelect();
$('.wide22').val($(".wide22 option:first").val()).niceSelect();

$(".date_picked_btn").click( function(){

    if($(this).parent().prev().hasClass('error')){
        $(this).parent().prev().prev().val('');
        $(this).parent().prev().prev().bootstrapMaterialDatePicker({
            format:"MM/DD/YYYY", 
            maxDate : new Date(),
            minDate: "01/12/2001",
            weekStart : 0, 
            time: false,
            switchOnClick: true,
            triggerEvent: 'customEvent' 
        });
        $(this).parent().prev().prev().trigger('customEvent');
    }else{
        $(this).parent().prev().val('');
        $(this).parent().prev().bootstrapMaterialDatePicker({
            format:"MM/DD/YYYY", 
            maxDate : new Date(),
            minDate: "01/12/2001",
            weekStart : 0, 
            time: false,
            switchOnClick: true,
            triggerEvent: 'customEvent' 
        });
        $(this).parent().prev().trigger('customEvent');
    }
    
});


var option = {
    details: ".address-info",
    detailsAttribute: "data-geo",
    country: "us",
    types: ["(regions)"]
};

$("#address")
    .geocomplete(option)
    .bind("geocode:result", function (event, result) {
        //console.log(result.types);
        if (result.formatted_address.split(",").length > 1) {
            arr = result.formatted_address.split(",");
            arr.pop();
            var result_arr = [];

            if(result.adr_address != undefined){
                $(result.adr_address).each(function(){
                    result_arr[ $(this).attr('class')] = $(this).text();
                });
                result_formatted = result_arr['postal-code'] +", "+result_arr['locality']+", "+result_arr['region'];
                $(event.target).val(result_formatted);
            }else{
                $(event.target).val(arr.toString());
            }               
        }

        $.each(result.types,function(key, value){
            if(value != 'postal_code'){
                $("#address").val('');
                $("#address").closest('form').validate().element('#address');
                $("#address").focus();
            }
        });
    });

    $("#hero-form-location")
        .geocomplete(option)
        .bind("geocode:result", function (event, result) {
            //console.log(result.types);
            if (result.formatted_address.split(",").length > 1) {
                arr = result.formatted_address.split(",");
                arr.pop();
                var result_arr = [];

                if(result.adr_address != undefined){
                    $(result.adr_address).each(function(){
                        result_arr[ $(this).attr('class')] = $(this).text();
                    });
                    result_formatted = result_arr['postal-code'] +", "+result_arr['locality']+", "+result_arr['region'];
                    $(event.target).val(result_formatted);
                    $('#address_ele').val($('.address-info').html());
                }else{
                    $(event.target).val(arr.toString());
                    $('#address_ele').val($('.address-info').html());
                }
            }

            $.each(result.types,function(key, value){
                if(value != 'postal_code'){
                    if($("#mainHeroForm").length > 0){
                        $("#hero-form-location").val('');
                        $("#mainHeroForm").validate().element('#hero-form-location');
                        $("#hero-form-location").focus();
                    }else{
                        $("#hero-form-location").val('');
                        $("#v2HeroForm").validate().element('#hero-form-location');
                        $("#hero-form-location").focus();
                    }
                   
                }
            });
        });


        // on change of  accident type dropdown
        $('.wide').on('change', function(){
            $('#casaType').val($(this).val());
            $("input[name=accident_type][value='" + $(this).val() + "']").prop('checked', true);
        });
        // on change of accident type radio
        $('input[type=radio][name=accident_type]').change(function() {
            $('select[name=accidentType]').val($(this).val());
            $('.wide').niceSelect('update');
            $('#casaType').val($(this).val());
        });

    // hamburger menu
    $('.first-button').on('click', function () {
        $('.animated-icon3').toggleClass('open');
    });
    




    // swipe carousel for most legal cases section
    // this is activated when screen is less than 992px
    $('.owl-carousel').owlCarousel({
        center: true,
        loop: true,
        margin: 10,
//        nav: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            }
        },
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true
    });
    $('.play').on('click', function () {
        $('.owl-carousel').trigger('play.owl.autoplay', [1000])
    })
    $('.stop').on('click', function () {
        $('.owl-carousel').trigger('stop.owl.autoplay')
    })

// show contact email div first in footer
if (Modernizr.mq('(max-width: 767px)')) {
    $('.contact-email').insertAfter('.footer-links div.footer-logo');
} else {
    $('.contact-email').insertAfter('.footer-links div:last-child');
}
//if (Modernizr.mq('(max-width: 425px)')) {
//    $('.start-eval .accident-type-logos h1').text('Injured In An Accident?');
//    $('#wizard_containerv2 .main_question').text('Injured In An Accident?');
//} else {
//    $('.start-eval .accident-type-logos h1').text('What\'s My Claim Worth?');
//    $('#wizard_containerv2 .main_question').text('What\'s My Claim Worth?');
//}
    // add active class to menu
    $('.navbar-nav li').click(function() {
        $('.navbar-nav li').removeClass("active");
        $(this).addClass("active");
    });

    $('ul.navbar-nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    // set common height to all cards (most common legal areas)
    var maxHeight = Math.max.apply(null, $(".common-legal-areas .card").map(function ()
    {
        return $(this).height();
    }).get());
    $(".common-legal-areas .card").height(maxHeight);

    // menu pop on responsive screens
    $('.btn-menu-toggle, .btn-close').on('click', function () {
        if ($('body').hasClass('menu-shown')) {
            $('body').removeClass('menu-shown');
        } else {
            $('body').addClass('menu-shown');
        }
    });
    // select dropdown value on menu item click
    $('.header-menu li:not(.dropdown), #mobile-menu li').on('click', function () {
        var caseType = $(this).children('a').text();
        if (caseType == 'Worker\'s Comp') {
            caseType = 'Workers Compensation';
        }
        $("input[name=accident_type][value='" + caseType + "']").prop('checked', true);
        $('select[name=accidentType]').val(caseType);
        $('.wide').niceSelect('update');
        $('#casaType').val(caseType);
        $('body').removeClass('menu-shown');

        focusTriggerEvaluation();
        

    });

    $('.header-m').on('click', function () {
        var caseType = $(this).text();
        if (caseType == 'Worker\'s Comp') {
            caseType = 'Workers Compensation';
        }
        $("input[name=accident_type][value='" + caseType + "']").prop('checked', true);
        $('select[name=accidentType]').val(caseType);
        $('.wide').niceSelect('update');
        $('#casaType').val(caseType);
        $('body').removeClass('menu-shown');
      
        focusTriggerEvaluation();
        
    });

    $('.populate_case_type').on('click', function () {
        var caseType = $(this).children().find('.card-title').text();//$(this).parent().next().text();
        if (caseType == 'Worker\'s Comp') {
            caseType = 'Workers Compensation';
        }

        if (caseType == 'Work-Related') {
            caseType = 'Work-Related Accident';
        }

        $("input[name=accident_type][value='" + caseType + "']").prop('checked', true);
        $('select[name=accidentType]').val(caseType);
        $('.wide').niceSelect('update');
        $('#casaType').val(caseType);
        $('body').removeClass('menu-shown');
        
        focusTriggerEvaluation();
      
    });

    $('.populate_from_footer').on('click', function () {
        var caseType = $(this).children('a').text();
        if (caseType == 'Worker\'s Comp') {
            caseType = 'Workers Compensation';
        }

        if (caseType == 'Work-Related') {
            caseType = 'Work-Related Accident';
        }

        $("input[name=accident_type][value='" + caseType + "']").prop('checked', true);
        $('select[name=accidentType]').val(caseType);
        $('.wide').niceSelect('update');
        $('#casaType').val(caseType);
        $('body').removeClass('menu-shown');

        focusTriggerEvaluation();     
        
    });

    $('#faq_cat').theiaStickySidebar({
        additionalMarginTop: 100
    });

});


$(window).resize(function() {
    var width = $(window).width();
    if (width < 767) {
        $('.contact-email').insertAfter('.footer-links div.footer-logo');
    } else {
        $('.contact-email').insertAfter('.footer-links div:last-child');
    }
//    if (width < 426) {
//        $('.start-eval .accident-type-logos h1').text('Injured In An Accident?');
//        $('#wizard_containerv2 .main_question').text('Injured In An Accident?');
//    } else {
//        $('.start-eval .accident-type-logos h1').text('What\'s My Claim Worth?');
//        $('#wizard_containerv2 .main_question').text('What\'s My Claim Worth?');
//    }
});

 $(window).scroll(function () {
     var scrollHeight = $(this).scrollTop();
//     console.log(scrollHeight);
    if ($(this).scrollTop() > 50)  /*height in pixels when the navbar becomes non opaque*/
    {
        $('.opaque-navbar').addClass('opaque');
    } else {
        $('.opaque-navbar').removeClass('opaque');
    }
    if (scrollHeight > 43) {
        $('.main-header-wrap .main-header').addClass('sml');
    } else {
        $('.main-header-wrap .main-header').removeClass('sml');
    }
    if (scrollHeight > 530) {
        $('header .btn-consultation').addClass('active');
    } else {
        $('header .btn-consultation').removeClass('active');
    }
});

/* //Xverify Verification
$(document).ready(function () {
    $.xVerifyService({
        services: {
            phone: {
                field: 'phone'
            },
        },
        submitType: 'onChange'
    });
}); */

function pleaseWait() {

    var options = {
        theme: "custom",
        // If theme == "custom" , the content option will be available to customize the logo
        content: '<img style="width:100px;" src="' + loaderGif + '" class="center-block">',
        message: '',
        backgroundColor: "#ffffff",
        textColor: "grey"
    };

    HoldOn.open(options);
}

function stopWaiting() {
    HoldOn.close();
}

function focusTriggerEvaluation(){
    $(window).scrollTop(0);
    if($("#accident_auto_wizard").length > 0){
        $("#accident_auto_wizard").valid();
        if($("#address").val() != ''){
            $("#accident_auto_wizard button[name='forward']").trigger('click');
        }
        $("#address").focus();
        
    }
    if($("#accident_auto_wizardv2").length){
        if($("#accident_auto_wizardv2").valid()){
            $("#submitv2").trigger('click');
        }else{
            $("#address").focus();
        }
    }

    if($("#mainHeroForm").length > 0){
        if($("#mainHeroForm").valid()){
            $("#mainHeroForm input[type='submit']").trigger('click');
        }else{
            $("#hero-form-location").focus();
        }
    }

    if($("#v2HeroForm").length > 0){
        if($("#v2HeroForm").valid()){
            $("#v2HeroForm input[type='submit']").trigger('click');
        }else{
            $("#hero-form-location").focus();
        }
    }
}