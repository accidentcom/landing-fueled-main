(function($) { 
	var servicetimeout = 10000;
	var tooltip_position =  '';
	var apiKey = '';
	var affiliateid = '';
	var subaffiliateid = '';
	var domainname = 'attorneys.accident.com';
	var emailcallstatus = false;
	var namecallstatus = false;
	var ipcallstatus = false;
	var phonecallstatus = false;
	var addresscallstatus = false;
	var formautosubmit = false;
	var captchaCall = false;
	var onsubmitbind = false;
	var onsubmitstring = '';
	var forms = new Array();
	var form_count = 1;
	var subscribeServices = '';
	var basicBaseUrl = 'www.xverify.com';
	var basicServerURL = basicBaseUrl + "/services/";
	var basicLoaderImagePath =  basicBaseUrl + "/images/loader.gif";
	var baseUrl = basicBaseUrl;
	var serverURL = basicServerURL;
	var loaderImagePath = basicLoaderImagePath;
	
	var myElementArray = Array();
	var nametimeout = 10000;
	var emailtimeout = 10000;
	var phonetimeout = 10000;
	var addresstimeout = 10000;
	var iptimeout = 10000;
	var is_mobile = false;
	var mistake_words= new Array();
	var service_captcha = new Array();
	var tooltip_class = 'tooltip';
	var tooltip_underprocess_class = 'xverify-ui-tooltip-underprocess';
	var tooltip_error_class = 'xverify-ui-tooltip-error';
	var tooltip_warning_class = 'xverify-ui-tooltip-warning';
	service_captcha["email"] = 0;
	service_captcha["phone"] = 0;
	service_captcha["address"] = 0;
	service_captcha["name"] = 0;
	service_captcha["ip"] = 0;
	var bypass_email = 0;
	var showCaptchaDiv = 'xverify_captcha_popup';
	var reCaptchaKey = '6LeLSNcSAAAAACXKRT7_5MwbKvRxUSXjnE05-nso';
	var verify_email_submit = false;
	var verify_phone_submit = false;
	var verify_address_submit = false;
	var elements_field_email = {'id':'email','name':'email','class':'xverify_email','tooltip_position':'right'}
	var elements_field_phone = {'id':'phone','name':'phone','class':'xverify_phone','tooltip_position':'right'}
	var elements_field_cell = {'id':'cell','name':'cell','class':'xverify_cell','tooltip_position':'right'}
	var elements_field_landline = {'id':'landline','name':'landline','class':'xverify_landline','tooltip_position':'right'}
	var elements_field_voip = {'id':'voip','name':'voip','class':'xverify_voip','tooltip_position':'right'}
	var elements_field_street = {'id':'street','name':'street','class':'xverify_street','tooltip_position':'right'}
	var elements_field_street_2 = {'id':'street_2','name':'street_2','class':'xverify_street_2','tooltip_position':'right'}
	var elements_field_city = {'id':'city','name':'city','class':'xverify_city','tooltip_position':'right'}
	var elements_field_state = {'id':'state','name':'state','class':'xverify_state','tooltip_position':'right'}
	var elements_field_zip = {'id':'zip','name':'zip','class':'xverify_zip','tooltip_position':'right'}
	var elements_field_button = '';
	var elements_field_esp_name = ''
	var elements_field_list_id = ''
	var elements_field_esp_contacts = {};
	var verified_email = '';
	var verified_email_elment = '';
	var userInputOption;
	var globalSubmitType = '';
	var change_class = false;
	var class_changed = false;
	$.fn.xvServiceExist = function(serviceName){
			var serviceStr = subscribeServices;
			var serviceArray = serviceStr.split(",");
			var return_value = false;
			for(var i=0; i<serviceArray.length; i++)
			{
			  if(serviceName == serviceArray[i])
			  {
				return_value = true;
				break;
			  }
			}
			return return_value;
		}
	function initalizeServicesURL(){
		var jsHost = 'http://';
		if(document.location.protocol == 'https:')
		{
			jsHost = 'https://';
		}
		baseUrl = jsHost + basicBaseUrl;
		serverURL = jsHost + basicServerURL;
		loaderImagePath = jsHost + basicLoaderImagePath;
	}
	function initalizeDomainnameParameters(){
		hostname = document.location.hostname;
		user_xverify_my_domain = $('#user_xverify_my_domain').val();
		if($.trim(user_xverify_my_domain) !='' && user_xverify_my_domain != undefined)
		{
			domainname = $.trim(user_xverify_my_domain);
		}
		else if( $.trim(hostname) !='' && hostname != undefined)
		{
			//domainname = $.trim(hostname);
		}
	}
	function initalizeAffiliatesParameters(){
		var allVars = getUrlVars();
		v1 = allVars['v1'];
		v2 = allVars['v2'];
		
		if( $.trim(v1) !='' && v1 != undefined)
		{
			affiliateid = $.trim(v1);
			
			if( $.trim(v2) !='' && v2 != undefined)
				subaffiliateid = v2;
		}
	}
	
	function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
		  hash = hashes[i].split('=');
		  vars.push(hash[0]);
		  vars[hash[0]] = hash[1];
		}
		return vars;
	}
		
	function getInputElementsByAttributeFromAllForms(attributeName, attributeValue,fieldType){
		fieldType = typeof fieldType !== 'undefined' ? fieldType : 'text';
	
		if(attributeName == 'class')
			return $('form :input[type="'+ fieldType +'"][' + attributeName + '*="' + attributeValue + '"]');
			
		return $('form :input[type="'+ fieldType +'"][' + attributeName + '="' + attributeValue + '"]');
	}
	
	function getInputElementsByAttributeFromSpecficForms(attributeName, attributeValue, formname){
		if(attributeName == 'class')
			return $(formname).find('input[type="text"][' + attributeName + '*="' + attributeValue + '"]');
		return $(formname).find('input[type="text"][' + attributeName + '="' + attributeValue + '"]');
	}
	
	function getInputElementsByAttributeFromSpecficFormsFieldType(attributeName, attributeValue, fieldType, formname){
		if(attributeName == 'class')
			return $(formname).find('input[type="'+ fieldType +'"][' + attributeName + '*="' + attributeValue + '"]');
		return $(formname).find('input[type="'+ fieldType +'"][' + attributeName + '="' + attributeValue + '"]');
	}
	
	function bindAffilateInputFields(){
		
		//----------------- get Affiliate Input field value starts here-------------------------//
		var inputFields = getInputElementsByAttributeFromAllForms('id', 'v1','hidden');
		
		if(affiliateid == '' && inputFields.length > 0)
		{
			affiliateid = inputFields.val();
		}
		
		if(affiliateid == '')
		{
			var inputFields = getInputElementsByAttributeFromAllForms('name', 'v1','hidden');
		}
		
		if(affiliateid == '' && inputFields.length > 0)
		{
			affiliateid = inputFields.val();
		}
		
		if(affiliateid == '')
		{
			var inputFields = getInputElementsByAttributeFromAllForms('class', 'xverify_v1','hidden');
		}
		
		if(affiliateid == '' && inputFields.length > 0)
		{
			affiliateid = inputFields.val();
		}
			
		//----------------- Affiliate Input field value ends here-------------------------//
	
		//----------------- get Sub Affiliate Input field value starts here-------------------------//
		var inputFields = getInputElementsByAttributeFromAllForms('id', 'v2','hidden');
		
		if(subaffiliateid == '' && inputFields.length > 0)
		{
			subaffiliateid = inputFields.val();
		}
	
		if(subaffiliateid == '')
		{
			var inputFields = getInputElementsByAttributeFromAllForms('name', 'v2','hidden');
		}
		
		if(subaffiliateid == '' && inputFields.length > 0)
		{
			subaffiliateid = inputFields.val();
		}
		if(subaffiliateid == '')
		{
			var inputFields = getInputElementsByAttributeFromAllForms('class', 'xverify_v2','hidden');
		}
		
		if(subaffiliateid == '' && inputFields.length > 0)
		{
			subaffiliateid = inputFields.val();
		}
	
		//----------------- Sub Affiliate Input field value ends here-------------------------//
	}

	function initializeVariables()
	{
		//var positions = { my: 'left center', at: 'right+20 center',"collision": "none" };
		tooltip_position =  { my: 'left center', at: 'right+10 center',"collision": "fit" }; // ok right position
		//tooltip_position =  { my: 'center top+10', at: 'center bottom',"collision": "fit" }; // ok bottom position
		//tooltip_position =  { my: 'center bottom-10', at: 'center top',"collision": "fit" }; // ok top position
		//tooltip_position =  { my: 'right center', at: 'left-10 center',"collision": "fit" }; // ok left position
		
		apiKey = $.getApiKey();
		subscribeServices = $.getServices();
	
	}
	function getTooltipPosition(position)
	{
		var tooltip_position =  { my: 'left center', at: 'right+10 center',"collision": "fit" };
		
		if(position == 'right')
		{
			tooltip_position =  { my: 'left center', at: 'right+10 center',"collision": "fit" }; // ok right position
		}
		else if(position == 'bottom')
		{
			change_class = true;
			tooltip_position =  { my: 'center top+10', at: 'center bottom',"collision": "fit" }; // ok bottom position
		}
		else if(position == 'top')
		{
			change_class = true;
			tooltip_position =  { my: 'center bottom-10', at: 'center top',"collision": "fit" }; // ok top position
		}
		else if(position == 'left')
		{
			change_class = true;
			tooltip_position =  { my: 'right center', at: 'left-10 center',"collision": "fit" }; // ok left position
		}
		css_data = $(".ui-tooltip-content::after").css('display','none');
		
		if(class_changed == false && change_class == true)
		{
			class_changed = true;$("head").append('<style type="text/css"></style>');
			var newStyleElement = $("head").children(':last');
			newStyleElement.html('.xverify-ui-tooltip-error .ui-tooltip-content::after{display:none;}.xverify-ui-tooltip-warning .ui-tooltip-content::after{display:none;}.xverify-ui-tooltip-underprocess .ui-tooltip-content::after{display:none;}');
		}
		return tooltip_position;
	}
	function includeFiles()
	{
		/*$("body").append("<div id='"+showCaptchaDiv+"'></div>");
		$.getScript(baseUrl+'/sharedjs/recaptcha_ajax.js',function(data, textStatus, jqxhr){
		});
		
		var dialogOptions = {autoOpen : false, position : {my: "center",at: "center"}, title : '', draggable : false, width : 350, 
			height : 200, resizable : false, showTitleBar: false, modal : true, dialogClass: 'ui-gray-dialog'};
		initializeDialog(showCaptchaDiv, dialogOptions);*/
	}
	function captchaResponseHandler(servicename,field)
	{
		$("#recaptcha_area").append("<div style='text-align:center'><button name='recaptcha_response_field_button' id='recaptcha_response_field_button' class='recaptcha_response_class_button' type='button'>Verify</button></div>");
		$('#recaptcha_response_field').keypress(function(event) {
		  if ( event.which == 13 ) {
			 verifyCaptchaRequest(servicename,field);
		   }
		});
		$('#recaptcha_response_field_button').click(function(event) {
			verifyCaptchaRequest(servicename,field);
		});
	
	}
	function verifyCaptchaRequest(servicename,field) {
		if(captchaCall === false)
		{
			
			var url = serverURL + "verify_captcha/?type=json&";
			var recaptcha_response_field = $( "#recaptcha_response_field" ).val();
			var recaptcha_challenge_field = $("#recaptcha_challenge_field" ).val();
			if($.trim(recaptcha_response_field) == '')
			{
				alert("Please enter captcha value")
				return ;
			}
			url = url + "callback=?";
			captchaCall = true;
			$.getJSON(
					  url, {"value" : recaptcha_response_field,"code" : recaptcha_challenge_field,"api_key":apiKey,'service_name':servicename},
					  function(json){
						  var status = json["status"];
						  captchaCall = false;
						  if(status == 'captcha_error')
						  {
							  Recaptcha.reload();
							  alert(json["message"]);
						  }
						  else
						  {
							  service_captcha[servicename] = 1;
							   field.trigger('change');
							  $("#"+showCaptchaDiv ).dialog('close');
						  }
			});
		}
	}
	function showRecaptcha(element_id,key,servicename,field){
		$("#"+showCaptchaDiv ).dialog('open');
		Recaptcha.destroy();
		Recaptcha.create(key,
				 element_id,
				{
				  theme: "red",
				  callback:function() {captchaResponseHandler(servicename,field) }
				}
			  );
		}
	function initializeDialog(containerNodeId, dialogOptions){
		$("#" + containerNodeId).dialog(dialogOptions);
		if(dialogOptions.hasOwnProperty('showTitleBar') == true && dialogOptions.showTitleBar  == false){
			$('#' + containerNodeId).siblings('.ui-dialog-titlebar').hide();
		}
	}	
	function initializeTimeOuts()
	{
		var gettimeoutstring = '';
		functionstatus  = $.isFunction($.getServicesTimeOut);
		if(functionstatus)
		{
			gettimeoutstring = $.getServicesTimeOut();
			var serviceTimeOutArray = gettimeoutstring.split(",");
			var timeoutvalue = '';
			for(var i=0; i<serviceTimeOutArray.length; i++)
			{
				timeoutvalue = serviceTimeOutArray[i].split(":");
				if(timeoutvalue[0] == 'email')
				{
					emailtimeout = timeoutvalue[1] * 1000;
					emailtimeout = emailtimeout + 2000;
				}
				else if(timeoutvalue[0] == 'phone')
				{
					phonetimeout = timeoutvalue[1] * 1000;
					phonetimeout = phonetimeout + 2000;
				}
				else if(timeoutvalue[0] == 'address')
				{
					addresstimeout = timeoutvalue[1] * 1000;
					addresstimeout = addresstimeout + 2000;
				}
				else if(timeoutvalue[0] == 'name')
				{
					nametimeout = timeoutvalue[1] * 1000;
					nametimeout = nametimeout + 2000;
				}
				else if(timeoutvalue[0] == 'ip_verify')
				{
					iptimeout = timeoutvalue[1];
					iptimeout = iptimeout + 2000;
				}
			}
		}
	}
	function bindRequiredInputFieldsByIdOrName(){
		
		if($(this).xvServiceExist('email'))
		{
			email_tooltip_position = getTooltipPosition(elements_field_email.tooltip_position);
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_email.id);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position ,tip_pos:elements_field_email.tooltip_position});
			inputElements.bind('change', emailChangeHandler);
	//		inputElements.bind('focus', fieldFocusHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_email.name);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position ,tip_pos:elements_field_email.tooltip_position});
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_email.id,'email');
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position ,tip_pos:elements_field_email.tooltip_position});
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("name",elements_field_email.name,'email');
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position ,tip_pos:elements_field_email.tooltip_position});
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
		}
		
		if($(this).xvServiceExist('phone'))
		{
			phone_tooltip_position = getTooltipPosition(elements_field_phone.tooltip_position);
			var inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_phone.name);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.bind('change', phoneChangeHandler);
					
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_phone.id);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', phoneChangeHandler);
			inputElements.bind('change', phoneChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_phone.name,'tel');
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.bind('change', phoneChangeHandler);
					
			inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_phone.id,'tel');
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', phoneChangeHandler);
			inputElements.bind('change', phoneChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_cell.name);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', cellChangeHandler);
			inputElements.bind('change', cellChangeHandler);
					
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_cell.id);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', cellChangeHandler);
			inputElements.bind('change', cellChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_landline.name);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', landlineChangeHandler);
			inputElements.bind('change', landlineChangeHandler);
					
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_landline.id);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', landlineChangeHandler);
			inputElements.bind('change', landlineChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_voip.name);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', voipChangeHandler);
			inputElements.bind('change', voipChangeHandler);
					
			var inputElements = getInputElementsByAttributeFromAllForms("id",  elements_field_voip.id);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', voipChangeHandler);
			inputElements.bind('change', voipChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_phone_1");
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_phone_1");
			inputElements.unbind('change', multiPhoneOtherFieldChangeHandler);
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_phone_2");
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_phone_2");
			inputElements.unbind('change', multiPhoneOtherFieldChangeHandler);
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_phone_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiPhoneToolTipBeforeShowHandler);
			inputElements.bind('change', multiPhoneChangeHandler);
	
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_phone_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiPhoneToolTipBeforeShowHandler);
			inputElements.unbind('change', multiPhoneChangeHandler);
			inputElements.bind('change', multiPhoneChangeHandler);
			
			
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_cell_1");
			inputElements.bind('change', multiCellPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_cell_1");
			inputElements.unbind('change', multiCellPhoneOtherFieldChangeHandler);
			inputElements.bind('change', multiCellPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_cell_2");
			inputElements.bind('change', multiCellPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_cell_2");
			inputElements.unbind('change', multiCellPhoneOtherFieldChangeHandler);
			inputElements.bind('change', multiCellPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_cell_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiCellPhoneToolTipBeforeShowHandler);
			inputElements.bind('change', multiCellPhoneChangeHandler);
	
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_cell_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiCellPhoneToolTipBeforeShowHandler);
			inputElements.unbind('change', multiCellPhoneChangeHandler);
			inputElements.bind('change', multiCellPhoneChangeHandler);
			
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_landline_1");
			inputElements.bind('change', multiLandlinePhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_landline_1");
			inputElements.unbind('change', multiLandlinePhoneOtherFieldChangeHandler);
			inputElements.bind('change', multiLandlinePhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_landline_2");
			inputElements.bind('change', multiLandlinePhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_landline_2");
			inputElements.unbind('change', multiLandlinePhoneOtherFieldChangeHandler);
			inputElements.bind('change', multiLandlinePhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_landline_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiLandlinePhoneToolTipBeforeShowHandler);
			inputElements.bind('change', multiLandlinePhoneChangeHandler);
	
			var inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_landline_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiLandlinePhoneToolTipBeforeShowHandler);
			inputElements.unbind('change', multiLandlinePhoneChangeHandler);
			inputElements.bind('change', multiLandlinePhoneChangeHandler);
		}
		if($(this).xvServiceExist('address'))
		{
			address_tooltip_position = getTooltipPosition(elements_field_street.tooltip_position);
			var inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_street.name);
			bindToolTipOnMultiInputElements(inputElements,{ message_position : address_tooltip_position,tip_pos:elements_field_street.tooltip_position },addressToolTipBeforeShowHandler);
			inputElements.bind('change', streetChangeHandler);
	
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_street.id);
			bindToolTipOnMultiInputElements(inputElements,{ message_position : address_tooltip_position,tip_pos:elements_field_street.tooltip_position },addressToolTipBeforeShowHandler);
			inputElements.unbind('change', streetChangeHandler);
			inputElements.bind('change', streetChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_city.name);
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_city.id);
			inputElements.unbind('change', addressOtherFieldChangeHandler);
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", elements_field_state.name);
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_state.id);
			inputElements.unbind('change', addressOtherFieldChangeHandler);
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name",elements_field_zip.name);
			bindToolTipOnInputElements(inputElements,{ message_position : address_tooltip_position,tip_pos:elements_field_street.tooltip_position });
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_zip.id);
			bindToolTipOnInputElements(inputElements,{ message_position : address_tooltip_position,tip_pos:elements_field_street.tooltip_position });
			inputElements.unbind('change', addressOtherFieldChangeHandler);
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name",elements_field_street_2.name);
			bindToolTipOnInputElements(inputElements,{ message_position : address_tooltip_position,tip_pos:elements_field_street.tooltip_position });
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", elements_field_street_2.id);
			bindToolTipOnInputElements(inputElements,{ message_position : address_tooltip_position,tip_pos:elements_field_street.tooltip_position });
			inputElements.unbind('change', addressOtherFieldChangeHandler);
			inputElements.bind('change', addressOtherFieldChangeHandler);
		}
		
		if($(this).xvServiceExist('ip_verify'))
		{
			var inputElements = getInputElementsByAttributeFromAllForms("name", "ipverify");
			bindToolTipOnInputElements(inputElements,{ message_position : tooltip_position });
			inputElements.bind('change', ipVerifyChangeHandler);
					
			var inputElements = getInputElementsByAttributeFromAllForms("id", "ipverify");
			bindToolTipOnInputElements(inputElements,{ message_position : tooltip_position });
			inputElements.unbind('change', ipVerifyChangeHandler);
			inputElements.bind('change', ipVerifyChangeHandler);
		}
		
		if($(this).xvServiceExist('name'))
		{
			var inputElements = getInputElementsByAttributeFromAllForms("name", "firstname");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },nameToolTipBeforeShowHandler);
			inputElements.bind('change', firstNameChangeHandler);
	
			var inputElements = getInputElementsByAttributeFromAllForms("id", "firstname");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },nameToolTipBeforeShowHandler);
			inputElements.unbind('change', firstNameChangeHandler);
			inputElements.bind('change', firstNameChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("name", "lastname");
			inputElements.bind('change', lastNameChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("id", "lastname");
			inputElements.unbind('change', lastNameChangeHandler);
			inputElements.bind('change', lastNameChangeHandler);
		}
	
	
	}
	
	
	function bindRequiredInputFieldsByClass(){
		if($(this).xvServiceExist('email'))
		{
			email_tooltip_position = getTooltipPosition(elements_field_email.tooltip_position);
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_email.class);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position,tip_pos:elements_field_email.tooltip_position });
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_email.class,'email');
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position,tip_pos:elements_field_email.tooltip_position });
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
		}
		if($(this).xvServiceExist('phone'))
		{
			phone_tooltip_position = getTooltipPosition(elements_field_phone.tooltip_position);
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_phone.class);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position,tip_pos:elements_field_phone.tooltip_position });
			inputElements.unbind('change', phoneChangeHandler);
			inputElements.bind('change', phoneChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_phone.class,'tel');
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.unbind('change', phoneChangeHandler);
			inputElements.bind('change', phoneChangeHandler);
					
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_cell.class);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.unbind('change', cellChangeHandler);
			inputElements.bind('change', cellChangeHandler);
					
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_landline.class);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.unbind('change', landlineChangeHandler);
			inputElements.bind('change',landlineChangeHandler);
					
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_voip.class);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.unbind('change', voipChangeHandler);
			inputElements.bind('change', voipChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_phone_1");
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_phone_2");
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_phone_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiPhoneToolTipBeforeShowHandler);
			inputElements.unbind('change', multiPhoneChangeHandler);
			inputElements.bind('change', multiPhoneChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_landline_1");
			inputElements.bind('change', multiLandlinePhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_landline_2");
			inputElements.bind('change', multiLandlinePhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_landline_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiLandlinePhoneToolTipBeforeShowHandler);
			inputElements.unbind('change', multiCellPhoneChangeHandler);
			inputElements.bind('change', multiLandlinePhoneChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_cell_1");
			inputElements.bind('change', multiCellPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_cell_2");
			inputElements.bind('change', multiCellPhoneOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_cell_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },multiCellPhoneToolTipBeforeShowHandler);
			inputElements.unbind('change', multiCellPhoneChangeHandler);
			inputElements.bind('change', multiCellPhoneChangeHandler);
		}
		
		if($(this).xvServiceExist('address'))
		{
			addess_tooltip_position = getTooltipPosition(elements_field_street.tooltip_position);
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_street.class);
			bindToolTipOnMultiInputElements(inputElements,{ message_position : addess_tooltip_position ,tip_pos:elements_field_street.tooltip_position},addressToolTipBeforeShowHandler);
			inputElements.unbind('change', streetChangeHandler);
			if(inputElements.length > 1)
			{
				for(loopi = 0 ; loopi < inputElements.length;loopi++)
				{
					inputElementDetail = $(inputElements[loopi]);
					if(inputElementDetail.hasClass(elements_field_street.class))
					{
						inputElementDetail.bind('change', streetChangeHandler);
					}
				}
			}
			else
			{
				inputElements.bind('change', streetChangeHandler);
			}
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_city.class);
			inputElements.unbind('change', addressOtherFieldChangeHandler);
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_state.class);
			inputElements.unbind('change', addressOtherFieldChangeHandler);
			inputElements.bind('change', addressOtherFieldChangeHandler);
	
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_zip.class);
			bindToolTipOnInputElements(inputElements,{ message_position : addess_tooltip_position ,tip_pos:elements_field_street.tooltip_position});
			inputElements.unbind('change', addressOtherFieldChangeHandler);
			inputElements.bind('change', addressOtherFieldChangeHandler);
			
			var inputElements = getInputElementsByAttributeFromAllForms("class", elements_field_street_2.class);
			bindToolTipOnInputElements(inputElements,{ message_position : addess_tooltip_position ,tip_pos:elements_field_street.tooltip_position});
			inputElements.unbind('change', addressOtherFieldChangeHandler);
			inputElements.bind('change', addressOtherFieldChangeHandler);

		}
		
		if($(this).xvServiceExist('ip_verify'))
		{
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_ipverify");
			bindToolTipOnInputElements(inputElements,{ message_position : tooltip_position });		
			inputElements.unbind('change', ipVerifyChangeHandler);
			inputElements.bind('change', ipVerifyChangeHandler);
				}
		
		if($(this).xvServiceExist('name'))
		{
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_firstname");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : tooltip_position },nameToolTipBeforeShowHandler);
			inputElements.unbind('change', firstNameChangeHandler);
			inputElements.bind('change', firstNameChangeHandler);
	
			var inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_lastname");
			inputElements.unbind('change', lastNameChangeHandler);
			inputElements.bind('change', lastNameChangeHandler);
	
		}
	
	}
	
	/****************************************** Get value from specific form via field *****************************************/
	function getFiledValueByForm(form, fieldname)
	{
		value = ''
		fieldElement = getInputElementsByAttributeFromSpecficForms('id',fieldname,form)
		if(fieldElement.length == 0)
		{
			fieldElement = getInputElementsByAttributeFromSpecficForms('name',fieldname,form)
			if(fieldElement.length == 0)
			{
				fieldclass = 'xverify_' + fieldname;
				fieldElement = getInputElementsByAttributeFromSpecficForms('class',fieldclass,form)
			}
		}
			
		if(fieldElement.length > 0)
		{
			value = fieldElement.val();
		}
		
		return value;
	}
	
	
	function getFiledElementByForm(form, fieldname)
	{
		value = ''
		fieldElement = getInputElementsByAttributeFromSpecficForms('id',fieldname,form)
		if(fieldElement.length == 0)
		{
			fieldElement = getInputElementsByAttributeFromSpecficForms('name',fieldname,form)
			if(fieldElement.length == 0)
			{
				fieldclass = 'xverify_' + fieldname;
				fieldElement = getInputElementsByAttributeFromSpecficForms('class',fieldclass,form)
				if(fieldElement.length == 0)
				{
					fieldElement = getInputElementsByAttributeFromSpecficForms('class',fieldname,form)
				}
			}
		}
		return fieldElement;
	}
	/****************************************** Get value from specific form via field *****************************************/
	
	/******************************************* Email Change Handler Function ********************************/
	
	function emailChangeHandler(event){
		var field = $(event.target);
		var serviceStatus = field.attr('service');
		if(serviceStatus == 0)
		{
			return;
		}
		var xvverify = field.attr('xvverify');
		if(xvverify == 1)
		{
			xvverify_value = field.attr('xvverify_value');
			xv_current_field_value = field.val();
			if($.trim(xvverify_value) == $.trim(xv_current_field_value))
			{
				return;
			}
			else
			{
				field.attr('xvverify',0);
			}
		}
		var email = field.val();
		var parentForm = this.form;
		var tooltip = field.tooltip();
		var submitBtn = $(parentForm).find("input[type='submit']");
		tClassName = "."+tooltip.tooltip('option').tip;
		$( tClassName).tooltip( "disable" );
		tip_pos = field.attr('tip_pos');
		if(tip_pos == undefined) tip_pos = '';
		if(tip_pos != '') tip_pos = tip_pos + '-';
		myElementArray[tooltip.tooltip('option').tip] = 1;
		$(parentForm).attr('state','proccess');
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.attr('disabled', 'true');
		addDisableFormButtons(submitBtn);
		$( tClassName).tooltip( "enable" );
		if(checkEmailSyntax(email))
		{
			var spellCheck = checkDomainSpell(email);
			if( spellCheck == true)
			{
				$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
				console.log(tClassName);
				if(is_mobile == true){
					//field.css({'border':'inherit','border-color': 'inherit'});
					field.css({ 'border' : '', 'border-color' : '' });
					$(tClassName).tooltip({content: "Verifying..."});
				}
				else
				{
					$(tClassName).tooltip({content: "Verifying... "});//<img src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
				}
				
				$(tClassName).tooltip( "open" );
				$(tClassName).tooltip().off('focusout mouseleave');
			}
			else
			{
				xverifySuggestEmail(email,spellCheck,tooltip,submitBtn,parentForm,field);
				return;
			}
	//		field.attr('disabled', 'true');
		}
		else if($.trim(email) == '')
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter email address"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else
		{
			tClassName = "."+tooltip.tooltip('option').tip;
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Invalid Syntax"});
			}
			else
			{
				$(tClassName).tooltip({content: "Your email address is in the incorrect format"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		emailServiceRequest(email,tooltip,submitBtn,parentForm,field );
	}
	function checkDomainSpell(email)
	{
		emaildomainname = getDomainNameFromEmail(email);
		if(mistake_words[emaildomainname] == undefined)
		{
			return true;
		}
		else
		{
			newemail = email.replace(emaildomainname,mistake_words[emaildomainname]);
			return newemail;
		}
	}
	function xverifySuggestEmail(email,suggestedemail,tooltip,submitBtn,parentForm,field){
		var tip_pos = '';
		tip_pos = field.attr('tip_pos');
		if(tip_pos == undefined) tip_pos = '';
		if(tip_pos != '') tip_pos = tip_pos + '-';
		
		var tClassName = "."+tooltip.tooltip('option').tip;
		var html = "<div align = 'left'><b>Did you mean: </b> &nbsp;&nbsp;<a href='javascript:' alt='" + suggestedemail +"' class='xverify_suggest_email'>" + suggestedemail +"</a>&nbsp;&nbsp; OR &nbsp;&nbsp;<a href='javascript:' alt='" + email +"' class='xverify_suggest_email'>" + email +"</a></div>";
		
		
		$( tClassName).tooltip( "enable" );
		$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_warning_class});
		$(tClassName).tooltip({content:html});
		$(tClassName).tooltip( "open" );
		$(tClassName).tooltip().off('focusout mouseleave');
			$(".xverify_suggest_email").click(function() {
				email = $(this).attr('alt');
				field.val(email);
				$( tClassName).tooltip( "disable" );
				$( tClassName).tooltip( "enable" );
				$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
				if(is_mobile == true){
					//field.css({'border':'inherit','border-color': 'inherit'});
					field.css({ 'border' : '', 'border-color' : '' });
					$(tClassName).tooltip({content: "Verifying..."});
				}
				else
				{
					$(tClassName).tooltip({content: "Verifying... "});//<img src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
				}
				$(tClassName).tooltip( "open" );
				$(tClassName).tooltip().off('focusout mouseleave');
				emailServiceRequest(email,tooltip,submitBtn,parentForm,field,1);
				return false;
			});
	}
	
	function xverifyByPassEmail(email,tooltip,submitBtn,parentForm,field,message,valid_message,invalid_message,call_function){
		var tClassName = "."+tooltip.tooltip('option').tip;
		var tip_pos = '';
		tip_pos = field.attr('tip_pos');
		if(tip_pos == undefined) tip_pos = '';
		if(tip_pos != '') tip_pos = tip_pos + '-';
		var html = "<div align = 'left'><b>" + message +"</b> &nbsp;&nbsp;<a href='javascript:' alt='Yes' class='xverify_suggest_email' >"+ valid_message +"</a>&nbsp;&nbsp; OR &nbsp;&nbsp;<a href='javascript:' alt='No' class='xverify_suggest_email' >"+ invalid_message +"</a></div>";
		
		$( tClassName).tooltip( "enable" );
		$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_warning_class});
		$(tClassName).tooltip({content:html});
		$(tClassName).tooltip( "open" );
		$(tClassName).tooltip().off('focusout mouseleave');
		
			
			$(".xverify_suggest_email").click(function() {
				email_alt = $(this).attr('alt');
				if(email_alt == 'Yes')
				{
					field.unbind('change');
					service_captcha["email"] = 0;
					myElementArray[tooltip.tooltip('option').tip] = 0;
					$(parentForm).removeAttr('state');
					submitBtn.removeAttr('disabled');	
					removeDisableFormButtons();	
					$( tClassName).tooltip( "disable" );
					
					if(jQuery.trim(call_function) != '' )
					{
						functionstatus  = $.isFunction(window[call_function]);
						if(functionstatus)
						{
							var callbackfunction_args = []
							callbackfunction_args.status = 2;
							callbackfunction_args.responsecode = 2;
							window[call_function](callbackfunction_args);
						}
					}
					
					
				}
				else if(email_alt == 'No')
				{
					service_captcha["email"] = 0;
					myElementArray[tooltip.tooltip('option').tip] = 0;
					$(parentForm).removeAttr('state');
					$( tClassName).tooltip( "disable" );
				}
				return false;
			});
	}
	
	function xverifyByPassPhone(tooltip,bypass_message,message_valid,message_invalid,submitBtn,parentForm,call_function){
		var tClassName = "."+tooltip.tooltip('option').tip;
		var html = "<div align = 'left'><b>" + bypass_message +"</b> <br> <a href='javascript:' alt='Yes' class='xverify_bypass_phone' >"+ message_valid +"</a>&nbsp;&nbsp; OR &nbsp;&nbsp;<a href='javascript:' alt='No' class='xverify_bypass_phone' >"+ message_invalid +"</a></div>";
		$(tClassName).tooltip({content:html});
		$(tClassName).tooltip().off('focusout mouseleave');
		
		$(".xverify_bypass_phone").click(function() {
				phone_alt = $(this).attr('alt');
				
				if(phone_alt == 'Yes')
				{
					service_captcha["phone"] = 0;
					myElementArray[tooltip.tooltip('option').tip] = 0;
					$(parentForm).removeAttr('state');
					submitBtn.removeAttr('disabled');		
					$( tClassName).tooltip( "disable" );
					removeDisableFormButtons();
					if(jQuery.trim(call_function) != '' )
					{
						functionstatus  = $.isFunction(window[call_function]);
						if(functionstatus)
						{
							var callbackfunction_args = []
							callbackfunction_args.status = 2;
							callbackfunction_args.responsecode = 2;
							window[call_function](callbackfunction_args);
						}
					}
				}
				else if(phone_alt == 'No')
				{				
					service_captcha["phone"] = 0;
					myElementArray[tooltip.tooltip('option').tip] = 0;
					$(parentForm).removeAttr('state');
					$( tClassName).tooltip( "disable" );
				}
				return false;
		});
		
			
	}
	function xverifyByPassAddress(tooltip,bypass_message,valid_message,inavlid_message,submitBtn,fieldElements,call_function){
		var tClassName = "."+tooltip.tooltip('option').tip;
		var html = "<div align = 'left'><b>" + bypass_message +"</b> <br> <a href='javascript:' alt='Yes' class='xverify_bypass_address' >"+ valid_message +"</a>&nbsp;&nbsp; OR &nbsp;&nbsp;<a href='javascript:' alt='No' class='xverify_bypass_address' >"+ inavlid_message  +"</a></div>";
		$(tClassName).tooltip({content:html});
		$(tClassName).tooltip().off('focusout mouseleave');
		
		$(".xverify_bypass_address").click(function() {
				address_alt = $(this).attr('alt');
				
				if(address_alt == 'Yes')
				{
					myElementArray[tooltip.tooltip('option').tip] = 0;
					parentForm = fieldElements.form;
					service_captcha["address"] = 0;
					$(parentForm).removeAttr('state');
					submitBtn.removeAttr('disabled');		
					$( tClassName).tooltip( "disable" );
					removeDisableFormButtons();
					
					var streetElement = fieldElements.street;
					var zipElement = fieldElements.zip;
					var cityElement = fieldElements.city;
					var stateElement = fieldElements.state;
					
					if(streetElement.length > 0) {streetElement.unbind('change')};
					if(zipElement.length > 0) {zipElement.unbind('change')};
					if(cityElement.length > 0) {cityElement.unbind('change')};
					if(stateElement.length > 0) {stateElement.unbind('change')};
				
					if(jQuery.trim(call_function) != '')
					{
						functionstatus  = $.isFunction(window[call_function]);
						if(functionstatus)
						{
							var callbackfunction_args = []
							callbackfunction_args.status = 2;
							callbackfunction_args.responsecode = 2;
							window[call_function](callbackfunction_args);
						}
					}	
				}
				else if(address_alt == 'No')
				{
					service_captcha["address"] = 0;
					myElementArray[tooltip.tooltip('option').tip] = 0;
					parentForm = fieldElements.form;				
					$(parentForm).removeAttr('state');
					$( tClassName).tooltip( "disable" );
				}
				return false;
		});
	}

	/******************************************* Email Change Handler Function ********************************/
	
	function formButtonCheckHandler(tooltip,submitBtn,parentForm)
	{
		var fire_form = true;
		for (var key in myElementArray)
			{
				if(myElementArray[key] == 1)
					{
						fire_form = false;
						if(submitBtn.length > 0 && formautosubmit == false)
						{
							submitBtn.attr('disabled', 'true');
							addDisableFormButtons(submitBtn);
						}
						
						$(parentForm).attr('state','proccess');
						break;
					}
					else
					{
						submitBtn.removeAttr('disabled');
						removeDisableFormButtons();
					}
			}
		if(fire_form == true && formautosubmit == true)
		{
			$(parentForm).attr('state','ok');
			$(parentForm).submit();
		}
	}
	/******************************************* Phone Change Handler Function ********************************/
	function cellChangeHandler(event){
		phoneChangeHandler(event,'cell');
	}
	function landlineChangeHandler(event){
		phoneChangeHandler(event,'landline');
	}
	function voipChangeHandler(event){
		phoneChangeHandler(event,'voip');
	}
	function phoneChangeHandler(event,phoneType){ 
		var field = $(event.target);
		var serviceStatus = field.attr('service');
		if(serviceStatus == 0)
		{
			return;
		}
		
		var xvverify = field.attr('xvverify');
		if(xvverify == 1)
		{
			xvverify_value = field.attr('xvverify_value');
			xv_current_field_value = field.val();
			if($.trim(xvverify_value) == $.trim(xv_current_field_value))
			{
				return;
			}
			else
			{
				field.attr('xvverify',0);
			}
		}
		var phone = field.val();
		var parentForm = $(field).closest('form');
		var tooltip = field.tooltip();
		var submitBtn = $(parentForm).find("input[type='submit']");
		var tip_pos = '';
		tip_pos = field.attr('tip_pos');
		if(tip_pos == undefined) tip_pos = '';
		if(tip_pos != '') tip_pos = tip_pos + '-';
		myElementArray[tooltip.tooltip('option').tip] = 1;
		$(parentForm).attr('state','proccess');
		
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.attr('disabled', 'true');
		addDisableFormButtons(submitBtn);
		tClassName = "."+tooltip.tooltip('option').tip;
		$( tClassName).tooltip( "disable" );
		
		if(phoneType == undefined)
		{
			phoneType = '';
		}
		$( tClassName).tooltip( "enable" );
		if(checkPhoneSyntax(phone))
		{
			$( tClassName).tooltip( "enable" );
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
			if(is_mobile == true){
				//field.css({'border':'inherit','border-color': 'inherit'});
				field.css({ 'border' : '', 'border-color' : '' });
				$(tClassName).tooltip({content: "Verifying..."});
			}
			else
			{
				$(tClassName).tooltip({content: "Verifying... "});//<img height21px width56px src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
		}
		else if($.trim(phone) == '')
		{
			tClassName = "."+tooltip.tooltip('option').tip;
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter phone number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){ 
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Invalid Syntax"});
			}
			else
			{
				$(tClassName).tooltip({content: "Invalid Phone Syntax"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		phoneServiceRequest(phone,tooltip,submitBtn,phoneType,parentForm,field);
	}
	/******************************************* Phone Change Handler Function ********************************/
	
	/******************************************* IP Change Handler Function ********************************/
	function ipVerifyChangeHandler(event){
		var field = $(event.target);
		var serviceStatus = field.attr('service');
		if(serviceStatus == 0)
		{
			return;
		}
		var ip_value = field.val();
		var parentForm = this.form;
		var tooltip = field.tooltip();
		var tip_pos = '';
		var submitBtn = $(parentForm).find("input[type='submit']");
		
		$(parentForm).attr('state','proccess');
		
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.attr('disabled', 'true');
		addDisableFormButtons(submitBtn);
		tClassName = "."+tooltip.tooltip('option').tip;
		$( tClassName).tooltip( "disable" );
		$( tClassName).tooltip( "enable" );
		ipsyntaxvalue = checkIpVerifySyntax(ip_value);
		if(ipsyntaxvalue == true)
		{
			//$(tClassName).remove
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
			if(is_mobile == true){
				//field.css({'border':'inherit','border-color': 'inherit'});
				field.css({ 'border' : '', 'border-color' : '' });
				$(tClassName).tooltip({content: "Verifying..."});
			}
			else
			{
				$(tClassName).tooltip({content: "Verifying... "});//<img src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
		}
		else if($.trim(ip_value) == '')
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter ip address"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){ 
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: ipsyntaxvalue});
			}
			else
			{
				$(tClassName).tooltip({content:ipsyntaxvalue});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		ipVerifyServiceRequest(ip_value,tooltip,submitBtn,parentForm ,field);
	}
	/******************************************* IP Change Handler Function ********************************/
	
	/******************************************* Name Handler Function ********************************/
	function nameToolTipBeforeShowHandler(event)
	{
		var element = $(event.currentTarget);
		//element = event.currentTarget;
		var parentForm = $(element).closest('form');
		var firstNameElement = getFiledElementByForm(parentForm,'firstname');
		var lastNameElement = getFiledElementByForm(parentForm,'lastname');
		
		var firstname = firstNameElement.val();
		var lastname = lastNameElement.val();
		if( firstname == '' || lastname == '')
		{
			event.preventDefault();
			return;
		}
	}
	function lastNameChangeHandler(event)
	{
		var field = $(event.target);
		var parentForm = this.form;
		var firstNameElement = getFiledElementByForm(parentForm,'firstname');
		firstNameElement.trigger('change'); 
	}
	function firstNameChangeHandler(event){
		var inputfield = $(event.target);
		var serviceStatus = inputfield.attr('service');
		if(serviceStatus == 0)
		{
			return;
		}
		var firstname = inputfield.val();
		var parentForm = this.form;
		var tooltip = inputfield.tooltip();
		var submitBtn = $(parentForm).find("input[type='submit']");
		var lastname ='';
		var lastNameElement = '';
		
		$(parentForm).attr('state','proccess');
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.attr('disabled', 'true');
		addDisableFormButtons(submitBtn);
		tClassName = "."+tooltip.tooltip('option').tip;
		$( tClassName).tooltip( "disable" );
		tip_pos = inputfield.attr('tip_pos');
		if(tip_pos == undefined) tip_pos = '';
		/***************************** Code For Require Fields ****************************************/
		if($.trim(firstname) != '')
		{
			lastNameElement = getFiledElementByForm(parentForm,'lastname');
			lastname = lastNameElement.val();
		}
		
		/***************************** Code For Require Fields ****************************************/
		if($.trim(firstname) == '')
		{
			/*$( tClassName).tooltip( "enable" );
			$(tClassName).tooltip({ "ui-tooltip":tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter first name"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			*/return;
		}
		else if($.trim(lastname) == '')
		{
			/*$( tClassName).tooltip( "enable" );
			$(tClassName).tooltip({ "ui-tooltip":tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter last name"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');*/
			return;
		}
		else
		{
			$( tClassName).tooltip( "enable" );
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
			if(is_mobile == true){
				//field.css({'border':'inherit','border-color': 'inherit'});
				field.css({ 'border' : '', 'border-color' : '' });
				$(tClassName).tooltip({content: "Verifying..."});
			}
			else
			{
				$(tClassName).tooltip({content: "Verifying... "});//<img src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
		}
		
		names = new Object();
		names.firstname = firstname;
		names.lastname = lastname;
	
		elements = new Object();
		elements.firstname = inputfield;
		elements.lastname = lastNameElement;
		elements.form = parentForm;
	
		nameVerifyServiceRequest(names,tooltip,submitBtn,elements );
	}
	
	/******************************************* Name Handler Function ********************************/
	
	
	/******************************************* Address Handler Function ********************************/
	
	function addressToolTipBeforeShowHandler(event)
	{
		var inputfield = $(event.currentTarget);
		var parentForm = $(inputfield).closest('form');
		zipElement = getFiledElementByForm(parentForm,elements_field_zip.id);
		street2Element = getFiledElementByForm(parentForm,elements_field_street_2.id);
		if(zipElement.length > 1 || street2Element.length > 1)
		{
			streetData = getStreetDataElementByField(inputfield,parentForm,zipElement,street2Element)
			zipElement = streetData.zipElement;
			street2Element = streetData.street2Element;
		}
		
		var street = inputfield.val();
		var zip = zipElement.val();
		
		if( street == '' || zip == '')
		{
			event.preventDefault();
			return;
		}
	}
	
	function addressOtherFieldChangeHandler(event)
	{
		var field = $(event.target);
		var parentForm = this.form;
		
		var streetElement = getFiledElementByForm(parentForm,elements_field_street.id);
		var zipElement = getFiledElementByForm(parentForm,elements_field_zip.id);
		var street2Element = getFiledElementByForm(parentForm,elements_field_street_2.id);
		var fieldIndex = -1;
		fieldId = field.attr('id');
		fieldName = field.attr('name');
		fieldText = field.get( 0 );
		streetElementData = [];
		loopEleCount = 0;
		if(streetElement.length > 1)
		{
			for(loopi = 0 ; loopi < streetElement.length;loopi++)
			{
				inputElementDetail = $(streetElement[loopi]);
				if(inputElementDetail.hasClass(elements_field_street_2.class))
				{
				
				}
				else
				{
					streetElementData[loopEleCount] = inputElementDetail;
					loopEleCount++;
				}
			}
			streetElement = streetElementData;
		}	
		if(zipElement.length > 1 || street2Element.length > 1)
		{
			for(loopi = 0 ; loopi < zipElement.length;loopi++)
			{
				inputElementDetail = $(zipElement[loopi]);
				inputElementDetailId = inputElementDetail.attr('id');
				inputElementDetailName = inputElementDetail.attr('name');
				inputElementDetailClass = inputElementDetail.attr('class');
				inputElementText = inputElementDetail.get( 0 );
				
				if(typeof fieldId !== 'undefined' && fieldId == inputElementDetailId)
				{
					fieldIndex = loopi;
					break;
				}
				if(typeof fieldName !== 'undefined' && fieldName == inputElementDetailName)
				{
					fieldIndex = loopi;
					break;
				}
				if(typeof fieldId === 'undefined' && typeof fieldName === 'undefined' )
				{
					if(fieldText == zipElement[loopi])
					{
						fieldIndex = loopi;
						break;
					}
				}
			}
			
			if(fieldIndex == -1)
			{
				for(loopi = 0 ; loopi < street2Element.length;loopi++)
				{
					
					inputElementDetail = $(street2Element[loopi]);
					inputElementDetailId = inputElementDetail.attr('id');
					inputElementDetailName = inputElementDetail.attr('name');
					inputElementDetailClass = inputElementDetail.attr('class');
					inputElementText = inputElementDetail.get( 0 );
					
					if(typeof fieldId !== 'undefined' && fieldId == inputElementDetailId)
					{
						fieldIndex = loopi;
						break;
					}
					if(typeof fieldName !== 'undefined' && fieldName == inputElementDetailName)
					{
						fieldIndex = loopi;
						break;
					}
					if(typeof fieldId === 'undefined' && typeof fieldName === 'undefined' )
					{
						if(fieldText == inputElementText)
						{
							fieldIndex = loopi;
							break;
						}
					}
				}
			}
			if(fieldIndex == -1)
			{
				return ;
			}
			else
			{
				lastElement = $(streetElement[fieldIndex]);
				lastElement.trigger('change');
			}
		}
		else
		{
			if(street2Element.length>1 && (streetElement.attr('id') == street2Element.attr('id') || streetElement.attr('name') == street2Element.attr('name')))
			{
				return ;
			}
			
			streetElement.trigger('change');
		}
	}
	function getStreetDataElementByField(inputfield,parentForm,zipElement,street2Element)
	{
		var streetData = {}
		streetData.zipElement = null;
		streetData.street2Element = null;
		fieldId = inputfield.attr('id');
		fieldName = inputfield.attr('name');
		fieldText = inputfield.get( 0 );
		fieldIndex = -1;
		var streetElement = getFiledElementByForm(parentForm,elements_field_street.id);
		var zipElement = getFiledElementByForm(parentForm,elements_field_zip.id);
		var street2Element = getFiledElementByForm(parentForm,elements_field_street_2.id);
		streetElementData = [];
		if(streetElement.length > 1)
		{
			loopEleCount = 0;
			for(loopi = 0 ; loopi < streetElement.length;loopi++)
			{
				inputElementDetail = $(streetElement[loopi]);
				if(inputElementDetail.hasClass(elements_field_street_2.class))
				{
				
				}
				else
				{
					streetElementData[loopEleCount] = inputElementDetail;
					loopEleCount++;
				}
			}
			streetElement = streetElementData;
		}
		else
		{
			streetElementData[0] =  streetElement;
			streetElement = streetElementData;
		}
		
		for(loopi = 0 ; loopi < streetElement.length;loopi++)
		{
			inputElementDetail = streetElement[loopi];
			inputElementDetailId = inputElementDetail.attr('id');
			inputElementDetailName = inputElementDetail.attr('name');
			inputElementDetailClass = inputElementDetail.attr('class');
			inputElementText = inputElementDetail.get( 0 );
			
			if(typeof fieldId !== 'undefined' && fieldId == inputElementDetailId)
			{
				fieldIndex = loopi;
				break;
			}
			if(typeof fieldName !== 'undefined' && fieldName == inputElementDetailName)
			{
				fieldIndex = loopi;
				break;
			}
			if(fieldText == inputElementText)
			{
				fieldIndex = loopi;
				break;
			}
			if(typeof fieldId === 'undefined' && typeof fieldName === 'undefined' )
			{
				if(fieldText == inputElementText)
				{
					fieldIndex = loopi;
					break;
				}
			}
		}
		
		if(zipElement.length == street2Element.length)
		{
			streetData.zipElement = $(zipElement[fieldIndex]);
			streetData.street2Element = $(street2Element[fieldIndex]);
		}
		else if(zipElement.length >  1 &&  street2Element.length == 0)
		{
			streetData.zipElement = $(zipElement[fieldIndex]);
		}
		else
		{
			streetData.zipElement = $(zipElement[fieldIndex]);
			streetData.street2Element = $(zipElement[fieldIndex]);
		}
		return streetData;
	}
	function streetChangeHandler(event){
		var inputfield = $(event.target);
		var serviceStatus = inputfield.attr('service');
		var parentForm = this.form;
		var zipElement = '';
		if(serviceStatus == 0)
		{
			return;
		}
		var xvverify = inputfield.attr('xvverify');
		if(xvverify == 1)
		{
			inputzipfield = getFiledElementByForm(parentForm,elements_field_zip.id);
			inputstreet2field = getFiledElementByForm(parentForm,elements_field_street_2.id);
			xvverify_value = inputfield.attr('xvverify_value');
			xvverify_value2 = inputzipfield.attr('xvverify_value');
			xvverify_value3 = inputstreet2field.attr('xvverify_value');
			
			xv_current_field_value = inputfield.val();
			xv_zip_current_field_value = inputzipfield.val();
			xv_street2_current_field_value = inputstreet2field.val();
			
			
			if($.trim(xvverify_value) == $.trim(xv_current_field_value) && $.trim(xvverify_value2) == $.trim(xv_zip_current_field_value))
			{
				if(inputstreet2field.length >0)
				{
					if($.trim(xvverify_value3) == $.trim(xv_street2_current_field_value))
					{
						return;
					}
					else
					{
						inputfield.attr('xvverify',0);
					}
				}
				else
				{
					return;
				}
			}
			else
			{
				inputfield.attr('xvverify',0);
			}
		}
		var streetaddress = inputfield.val();
		var parentForm = this.form;
		var submitBtn = $(parentForm).find("input[type='submit']");
		var zipElement = '';
		var cityElement = '';
		var stateElement = '';
		var tooltip = inputfield.tooltip();
		var ziptooltip = '';
		var zipcode = '';
		var city = '';
		var state = '';
		var tip_pos = '';
		tip_pos = inputfield.attr('tip_pos');
		if(tip_pos == undefined) tip_pos = '';
		if(tip_pos != '') tip_pos = tip_pos + '-';
		if(inputfield.attr('id') == elements_field_street_2.id || inputfield.attr('name') == elements_field_street_2.name || inputfield.hasClass(elements_field_street_2.class))
		{
			return;
		}
		zipElement = getFiledElementByForm(parentForm,elements_field_zip.id);
		street2Element = getFiledElementByForm(parentForm,elements_field_street_2.id);
		
		
		if(zipElement.length > 1 || street2Element.length > 1)
		{
			streetData = getStreetDataElementByField(inputfield,parentForm,zipElement,street2Element)
			zipElement = streetData.zipElement;
			street2Element = streetData.street2Element;
		}
		myElementArray[tooltip.tooltip('option').tip] = 1;
		$(parentForm).attr('state','proccess');
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.attr('disabled', 'true');
		addDisableFormButtons(submitBtn);
		tClassName = "."+tooltip.tooltip('option').tip;
		if(tClassName == '.undefined')
		{
			tClassName = "."+inputfield.attr('use_class');
		}
		$( tClassName).tooltip( "disable" );
		
		if(zipElement.length > 1)
		{
			for(loopi = 0 ; loopi < zipElement.length;loopi++)
			{
				inputElementDetail = $(zipElement[loopi]);
				ziptooltip = inputElementDetail.tooltip();
				tZipClassName = "."+ziptooltip.tooltip('option').tip;
				if(tZipClassName == '.undefined')
				{
					tZipClassName = "."+inputElementDetail.attr('use_class');
					//ziptooltip = $( tZipClassName).tooltip();
					//tZipClassName = "."+ziptooltip.tooltip('option').tip;
				}
				$( tZipClassName).tooltip( "disable" );
			}
		}
		else
		{
			zipcode = zipElement.val();
			ziptooltip = zipElement.tooltip();
			tZipClassName = "."+ziptooltip.tooltip('option').tip;
			if(tZipClassName == '.undefined')
			{
				tZipClassName = "."+zipElement.attr('use_class');
			}
			$( tZipClassName).tooltip( "disable" );
		}
		
		cityElement = getFiledElementByForm(parentForm,'city');
		city = cityElement.val();
		
		stateElement = getFiledElementByForm(parentForm,'state');
		state = stateElement.val();
		
		
		if(street2Element.length > 0)
		{
			if(street2Element.length > 1)
			{
				for(loopi = 0 ; loopi < street2Element.length;loopi++)
				{
					inputElementDetail = $(street2Element[loopi]);
					street2tooltip = inputElementDetail.tooltip();
					tStreet2ClassName = "."+street2tooltip.tooltip('option').tip;
					if(tStreet2ClassName == '.undefined')
					{
						tStreet2ClassName = "."+inputElementDetail.attr('use_class');
						//street2tooltip = $( tStreet2ClassName).tooltip();
						//tStreet2ClassName = "."+street2tooltip.tooltip('option').tip;
					}
					$( tStreet2ClassName).tooltip( "disable" );
				}
			}
			else
			{
				street2tooltip = street2Element.tooltip();
				tStreet2ClassName = "."+street2tooltip.tooltip('option').tip;
				if(tStreet2ClassName == '.undefined')
				{
					tStreet2ClassName = "."+street2Element.attr('use_class');
				}
				$( tStreet2ClassName).tooltip( "disable" );
			}
			
		}
		if( streetaddress == '' || zipcode == '')
		{
			//event.preventDefault();
			//return;
		}
		/***************************** Code For Require Fields ****************************************/
		$( tClassName).tooltip( "enable" );
		if($.trim(streetaddress) == '')
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter street address"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if($.trim(zipcode) == '')
		{
			$( tClassName).tooltip( "disable" );
			$( tZipClassName).tooltip( "enable" );
			
			$(tZipClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tZipClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tZipClassName).tooltip({content: "Please enter zip code"});
			}
			$(tZipClassName).tooltip( "open" );
			$(tZipClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else
		{
			//street2tooltip = street2Element.tooltip();
			//tStreet2ClassName = "."+street2tooltip.tooltip('option').tip;
			//$(tStreet2ClassName).tooltip({content: "Verifying... <img src='"+ loaderImagePath +"' style='vertical-align:middle'/>"});
			//$(tStreet2ClassName).tooltip( "open" );
			//$(tStreet2ClassName).tooltip().off('focusout mouseleave');
			
			if(street2Element.length > 0)
			{
				street2tooltip = street2Element.tooltip();
				tStreet2ClassName = "."+street2tooltip.tooltip('option').tip;
				$( tStreet2ClassName).tooltip( "disable" );
			}
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
			if(is_mobile == true){
				//field.css({'border':'inherit','border-color': 'inherit'});
				field.css({ 'border' : '', 'border-color' : '' });
				$(tClassName).tooltip({content: "Verifying..."});
			}
			else
			{
				$(tClassName).tooltip({content: "Verifying... "});//<img src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
		}
		
		address = new Object();
		address.street = streetaddress;
		if(street2Element.length > 0)
		{
			address.street = streetaddress + " " + street2Element.val();
		}
		address.zip = zipcode;
		address.city = city;
		address.state = state;
	
		elements = new Object();
		elements.street = inputfield;
		elements.street_2 = street2Element;
		elements.zip = zipElement;
		elements.city = cityElement;
		elements.state = stateElement;
		elements.form = parentForm;
		addressVerifyServiceRequest(address,tooltip,submitBtn,elements );
	}
	/******************************************* Address Handler Function ********************************/
	
	/*************************************** Multi Field Phone Change Handler  *****************************/
	function multiPhoneToolTipBeforeShowHandler(event)
	{
		var element =  $(event.currentTarget);
		var parentForm = $(element).closest('form');
		var phone_1_Element = getFiledElementByForm(parentForm,'multifield_phone_1');
		var phone_2_Element = getFiledElementByForm(parentForm,'multifield_phone_2');
		var phone_3_Element = getFiledElementByForm(parentForm,'multifield_phone_3');
		
		var phone_1 = phone_1_Element.val();
		var phone_2 = phone_2_Element.val();
		var phone_3 = phone_3_Element.val();
		
		if(phone_3 != '')
		{
		}
		else if( phone_1 == '' || phone_2 == '' || phone_3 == '')
		{
			//event.preventDefault();
			//return;
		}
	}
	function multiPhoneOtherFieldChangeHandler(event)
	{
		var field = $(event.target);
		var parentForm = this.form;
		var inputElement = getFiledElementByForm(parentForm,'multifield_phone_3');
		inputElement.trigger('change'); 
	}
	
	function multiPhoneChangeHandler(event){
		var inputfield = $(event.target);
		var serviceStatus = inputfield.attr('service');
		if(serviceStatus == 0)
		{
			return;
		}
		var streetaddress = inputfield.val();
		var parentForm = this.form;
		var tooltip = inputfield.tooltip();
		var submitBtn = $(parentForm).find("input[type='submit']");
		var phone_1_Element = '';
		var phone_2_Element = '';
		var phone_3_Element = '';
	
		var phone_1 = '';
		var phone_2 = '';
		var phone_3 = '';
		var tip_pos = '';
		tClassName = "."+tooltip.tooltip('option').tip;
		$( tClassName).tooltip( "disable" );
	
		$(parentForm).attr('state','proccess');
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.attr('disabled', 'true');
		addDisableFormButtons(submitBtn);
		phone_1_Element = getFiledElementByForm(parentForm,'multifield_phone_1');
		phone_1 = phone_1_Element.val();
		
		phone_2_Element = getFiledElementByForm(parentForm,'multifield_phone_2');
		phone_2 = phone_2_Element.val();
		
		phone_3_Element = getFiledElementByForm(parentForm,'multifield_phone_3');
		phone_3 = phone_3_Element.val();
		
		if( phone_1 == '' && phone_2 == '' && phone_3 == '')
		{
		}
		else if( phone_1 == '' || phone_2 == '' || phone_3 == '')
		{
			return;
		}
		
		phone = phone_1 + phone_2 + phone_3;
		
		var serviceStatus_1 = phone_1_Element.attr('service');
		var serviceStatus_2 = phone_2_Element.attr('service');
		var serviceStatus_3 = phone_3_Element.attr('service');
		
		if(serviceStatus_1 == 0 || serviceStatus_2 == 0 || serviceStatus_3 == 0)
		{
			return;
		}
		var xvverify = phone_3_Element.attr('xvverify');
		if(xvverify == 1)
		{
			xvverify_value = phone_3_Element.attr('xvverify_value');
			xv_current_field_value = phone;
			if($.trim(xvverify_value) == $.trim(xv_current_field_value))
			{
				return;
			}
			else
			{
				field.attr('xvverify',0);
			}
		}
		
		myElementArray[tooltip.tooltip('option').tip] = 1;
		$( tClassName).tooltip( "enable" );
		/***************************** Code For Require Fields ****************************************/
		if($.trim(phone_1) == '' || phone_1.length != 3)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid phone number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if($.trim(phone_2) == '' || phone_2.length != 3)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid phone number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if($.trim(phone_3) == '' || phone_3.length != 4)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid phone number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if(checkPhoneSyntax(phone))
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
			if(is_mobile == true){
				//field.css({'border':'inherit','border-color': 'inherit'});
				field.css({ 'border' : '', 'border-color' : '' });
				$(tClassName).tooltip({content: "Verifying..."});
			}
			else
			{
				$(tClassName).tooltip({content: "Verifying... "});//<img src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
		}
		else
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Invalid syntax"});
			}
			else
			{
				$(tClassName).tooltip({content: "Invalid phone syntax"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		phoneServiceRequest(phone,tooltip,submitBtn,'',parentForm,inputfield);
	}
	/*************************************** Multi Field Phone Change Handler  *****************************/
	
	
	/*************************************** Multi Field Cell Change Handler  *****************************/
	function multiCellPhoneToolTipBeforeShowHandler(event)
	{
		var element = $(event.currentTarget);
		var parentForm = $(element).closest('form');
		var phone_1_Element = getFiledElementByForm(parentForm,'multifield_cell_1');
		var phone_2_Element = getFiledElementByForm(parentForm,'multifield_cell_2');
		var phone_3_Element = getFiledElementByForm(parentForm,'multifield_cell_3');
		
		var phone_1 = phone_1_Element.val();
		var phone_2 = phone_2_Element.val();
		var phone_3 = phone_3_Element.val();
		
		if(phone_3 != '')
		{
		}
		else if( phone_1 == '' || phone_2 == '' || phone_3 == '')
		{
			event.preventDefault();
			return;
		}
	}
	function multiCellPhoneOtherFieldChangeHandler(event)
	{
		var field = $(event.target);
		var parentForm = this.form;
		var inputElement = getFiledElementByForm(parentForm,'multifield_cell_3');
		inputElement.trigger('change'); 
	}
	
	function multiCellPhoneChangeHandler(event){
		var inputfield = $(event.target);
		var serviceStatus = inputfield.attr('service');
		if(serviceStatus == 0)
		{
			return;
		}
		var streetaddress = inputfield.val();
		var parentForm = this.form;
		var tooltip = inputfield.tooltip();
		var submitBtn = $(parentForm).find("input[type='submit']");
		var phone_1_Element = '';
		var phone_2_Element = '';
		var phone_3_Element = '';
	
		var phone_1 = '';
		var phone_2 = '';
		var phone_3 = '';
		var tip_pos = '';
	
		$(parentForm).attr('state','proccess');
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.attr('disabled', 'true');
		addDisableFormButtons(submitBtn);
		tClassName = "."+tooltip.tooltip('option').tip;
		$( tClassName).tooltip( "disable" );
		phone_1_Element = getFiledElementByForm(parentForm,'multifield_cell_1');
		phone_1 = phone_1_Element.val();
		
		phone_2_Element = getFiledElementByForm(parentForm,'multifield_cell_2');
		phone_2 = phone_2_Element.val();
		
		phone_3_Element = getFiledElementByForm(parentForm,'multifield_cell_3');
		phone_3 = phone_3_Element.val();
		
		if( phone_1 == '' || phone_2 == '' || phone_3 == '')
		{
			return;
		}
		
		phone = phone_1 + phone_2 + phone_3;
		/***************************** Code For Require Fields ****************************************/
		$( tClassName).tooltip( "enable" );
		if($.trim(phone_1) == '' || phone_1.length != 3)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid cell number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if($.trim(phone_2) == '' || phone_2.length != 3)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid cell number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if($.trim(phone_3) == '' || phone_3.length != 4)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid cell number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if(checkPhoneSyntax(phone))
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
			if(is_mobile == true){
				//field.css({'border':'inherit','border-color': 'inherit'});
				field.css({ 'border' : '', 'border-color' : '' });
				$(tClassName).tooltip({content: "Verifying..."});
			}
			else
			{
				$(tClassName).tooltip({content: "Verifying... "});//<img src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
		}
		else
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Invalid Cell Phone Syntax"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		
		
		phoneServiceRequest(phone,tooltip,submitBtn,'cell',parentForm,inputfield);
	}
	/*************************************** Multi Field Cell Change Handler  *****************************/
	
	/*************************************** Multi Field Landline Change Handler  *****************************/
	function multiLandlinePhoneToolTipBeforeShowHandler(event)
	{
		var element = $(event.currentTarget);
		var parentForm = $(element).closest('form');
		var phone_1_Element = getFiledElementByForm(parentForm,'multifield_landline_1');
		var phone_2_Element = getFiledElementByForm(parentForm,'multifield_landline_2');
		var phone_3_Element = getFiledElementByForm(parentForm,'multifield_landline_3');
		
		var phone_1 = phone_1_Element.val();
		var phone_2 = phone_2_Element.val();
		var phone_3 = phone_3_Element.val();
		
		if(phone_3 != '')
		{
		}
		else if( phone_1 == '' || phone_2 == '' || phone_3 == '')
		{
			event.preventDefault();
			return;
		}
	}
	function multiLandlinePhoneOtherFieldChangeHandler(event)
	{
		var field = $(event.target);
		var parentForm = this.form;
		var inputElement = getFiledElementByForm(parentForm,'multifield_landline_3');
		inputElement.trigger('change'); 
	}
	
	function multiLandlinePhoneChangeHandler(event){
		var inputfield = $(event.target);
		var serviceStatus = inputfield.attr('service');
		if(serviceStatus == 0)
		{
			return;
		}
		var streetaddress = inputfield.val();
		var parentForm = this.form;
		var tooltip = inputfield.tooltip();
		var submitBtn = $(parentForm).find("input[type='submit']");
		var phone_1_Element = '';
		var phone_2_Element = '';
		var phone_3_Element = '';
	
		var phone_1 = '';
		var phone_2 = '';
		var phone_3 = '';
		var tip_pos = '';
	
		$(parentForm).attr('state','proccess');
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.attr('disabled', 'true');
		addDisableFormButtons(submitBtn);
		tClassName = "."+tooltip.tooltip('option').tip;
		$( tClassName).tooltip( "disable" );
		
		
		phone_1_Element = getFiledElementByForm(parentForm,'multifield_landline_1');
		phone_1 = phone_1_Element.val();
		
		phone_2_Element = getFiledElementByForm(parentForm,'multifield_landline_2');
		phone_2 = phone_2_Element.val();
		
		phone_3_Element = getFiledElementByForm(parentForm,'multifield_landline_3');
		phone_3 = phone_3_Element.val();
		
		if( phone_1 == '' || phone_2 == '' || phone_3 == '')
		{
			return;
		}
		
		phone = phone_1 + phone_2 + phone_3;
		/***************************** Code For Require Fields ****************************************/
		$( tClassName).tooltip( "enable" );
		
		if($.trim(phone_1) == '' || phone_1.length != 3)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid landline number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if($.trim(phone_2) == '' || phone_2.length != 3)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid landline number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if($.trim(phone_3) == '' || phone_3.length != 4)
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Input required"});
			}
			else
			{
				$(tClassName).tooltip({content: "Please enter valid landline number"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		else if(checkPhoneSyntax(phone))
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_underprocess_class});
			if(is_mobile == true){
				//field.css({'border':'inherit','border-color': 'inherit'});
				field.css({ 'border' : '', 'border-color' : '' });
				$(tClassName).tooltip({content: "Verifying..."});
			}
			else
			{
				$(tClassName).tooltip({content: "Verifying... "});//<img src='"+ loaderImagePath +"' style='vertical-align:middle;width56px;height:21px'/>
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
		}
		else
		{
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
				$(tClassName).tooltip({content: "Invalid syntax"});
			}
			else
			{
				$(tClassName).tooltip({content: "Invalid landline phone syntax"});
			}
			$(tClassName).tooltip( "open" );
			$(tClassName).tooltip().off('focusout mouseleave');
			return;
		}
		phoneServiceRequest(phone,tooltip,submitBtn,'landline',parentForm,inputfield);
	}
	/*************************************** Multi Field Phone Change Handler  *****************************/
	
	
	/****************************************** Field Tooltip Bind Function ************************************/
	function bindToolTipOnInputElements(inputFields, options){
		var positions;
		var tip_pos = '';
		if(options == null){
			 positions = { my: 'left center', at: 'right+20 center',"collision": "none" };
		}
		else
		{
			positions = options.message_position;
		}
		if(is_mobile == true)
		{
			//positions = { my: 'bottom center', at: 'right+10 left',"collision": "none" };
			positions = { my: 'left top', at: 'left bottom+3',"collision": "none" };
			cur_div_opc = 1;
		}
		if(typeof options.tip_pos  !== 'undefined' )
		{
			tip_pos = options.tip_pos;
		}
		var date = new Date();
		var serviceStatus = '';
		milisec =  date.getTime();
		for (var i = 0; i < inputFields.length; i++)
		{
			field = $(inputFields[i]);
			currentTitle = field.attr('title');
			serviceStatus = field.attr('service');	
			use_class = field.attr('use_class');
			
			if(use_class!=undefined) continue;
			
			//if(serviceStatus == 0)
			//{
			//	continue;
			//}
			milisec =  date.getTime();
			divid =  'div_' + milisec + i + '_loadingmsg';
			field.addClass(divid);
			$('.'+divid).tooltip({content: "",position:positions,tip:divid}).off('focusout mouseleave');
			if(currentTitle!=undefined)
			{
				field.attr('title',currentTitle);
			}
			else
			{
				field.attr('title','  ');
			}
			$('.'+divid).on( "tooltipclose",toolTipOnBeforeHideHandler);
			field.attr('use_class',divid);
			field.attr('tip_pos',tip_pos);
		}
	}
	
	function bindToolTipOnMultiInputElements(inputFields, options,toolTipShowHandler){
		var positions;
		var tip_pos = '';
		if(options == null){
			 positions = { my: 'left center', at: 'right+10 center',"collision": "none" };
		}
		else
		{
			positions = options.message_position;
		}
		if(is_mobile == true)
		{
			positions = { my: 'center top', at: 'center bottom+3',"collision": "none" };
			cur_div_opc = 1;
		}
		
		if(typeof options.tip_pos  !== 'undefined' )
		{
			tip_pos = options.tip_pos;
		}
		
		var date = new Date();
		var serviceStatus = '';
		milisec =  date.getTime();
		
		for (var i = 0; i < inputFields.length; i++)
		{
			field = $(inputFields[i]);
			currentTitle = field.attr('title');
			serviceStatus = field.attr('service');	
			use_class = field.attr('use_class');	
			
			if(use_class!=undefined) continue;
			
			if(serviceStatus == 0)
			{
				continue;
			}
			milisec =  date.getTime();
			divid =  'div_' + milisec + i + '_loadingmsg';
			field.addClass(divid);
			$('.'+divid).tooltip({content: "",position:positions,tip:divid}).off('focusout mouseleave');
			
			$('.'+divid).on( "tooltipclose",toolTipOnBeforeHideHandler);
			$('.'+divid).on( "tooltipopen",toolTipShowHandler);
			
			if(currentTitle!=undefined)
			{
				field.attr('title',currentTitle);
			}
			else
			{
				field.attr('title','  ');
			}
			field.attr('use_class',divid);
			field.attr('tip_pos',tip_pos);
		}
	}
	function toolTipOnBeforeHideHandler(event)
	{
		var element = $(event.currentTarget);
		var parentForm = $(element).closest('form');
		if($(parentForm).attr('state') == 'proccess')
		{
			event.preventDefault();
		}
	}
	function addToolTipDiv(divid){
		var loading_div = "<div class='"+tooltip_underprocess_class+"' id='"+divid+"'></div>";
		$("body").prepend(loading_div);
	}
	/****************************************** Field Change Handler Function ************************************/
	
	/*********************************************************** Syntax Check functions **********************************************/
	function checkEmailSyntax(str){
		if(isGmailAddress(str) == true)
		{
			str = removePlusFromEmailAddress(str);
		}
		 reEmail = new RegExp(/^([a-zA-Z0-9])+([a-zA-Z0-9\+._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/);
		 if (!reEmail.test(str)) {
		  return false;
		}
		 return true
	}
	function removePlusFromEmailAddress(Email)
	{
		var domain = Email.substring(Email.lastIndexOf('@')+1, Email.length);
		var emailHandle = Email.substring(0,Email.lastIndexOf('+'));
		if(emailHandle != '')
		{
			Email = emailHandle + '@' + domain ;
		}	
		return Email;
	}
	function isGmailAddress(str)
	{
		if(getDomainFromEmail(str) == 'gmail')
			return true;
		else
			return false;	
	}
	
	function getDomainFromEmail(Email){
		var atsign = Email.substring(Email.lastIndexOf('@')+1, Email.length);
		atsign = atsign.substring(0,atsign.lastIndexOf('.'));
		atsign.toLowerCase();
		return atsign;
	}
	
	function getDomainNameFromEmail(Email){
		var atsign = Email.substring(Email.lastIndexOf('@')+1, Email.length);
		atsign.toLowerCase();
		return atsign;
	}
		
	function checkPhoneSyntax(str){
		if(str.length < 10){
		   return false
		}
		var raw_number = str.replace(/[^0-9]/g,'');
		rePhoneNumber = new RegExp(/^[1-9]\d{3}\d{3}\d{4}$/);
		phoneNumberPattern = new RegExp(/^\d{3}[- ]?\d{3}[- ]?\d{4}$/);
		var regex1 = /^1?([2-9]..)([2-9]..)(....)$/;
		if (rePhoneNumber.test(str) || phoneNumberPattern.test(str) || regex1.test(raw_number)) {
		  return true;
		}
		return false;
	}
	
	function checkIpVerifySyntax (IPvalue) {
		errorString = "";
		theName = "IPaddress";
		var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
		var ipArray = IPvalue.match(ipPattern);
		if (IPvalue == "0.0.0.0")
			errorString = errorString + theName + ': '+IPvalue+' is a special IP address and cannot be used here.';
		else if (IPvalue == "255.255.255.255")
			errorString = errorString + theName + ': '+IPvalue+' is a special IP address and cannot be used here.';
		if (ipArray == null)
			errorString = errorString + theName + ': '+IPvalue+' is not a valid IP address.';
		else 
		{
			for (i = 0; i < 4; i++) {
				thisSegment = ipArray[i];
				if (thisSegment > 255) {
					errorString = errorString + theName + ': '+IPvalue+' is not a valid IP address.';
					i = 4;
				}
				if ((i == 0) && (thisSegment > 255)) {
					errorString = errorString + theName + ': '+IPvalue+' is a special IP address and cannot be used here.';
					i = 4;
				}
			}
		}
		extensionLength = 3;
		if (errorString == "")
			return true;
		else
			return errorString;
	}
	
	
	function checkNameField(first,last){
		first = $.trim( first );
		last = $.trim( last );
		if(first == '' || last == '' )
		{
		   return false
		}
		
		return true	
	}
	/*********************************************************** Syntax Check functions **********************************************/
	
	
	/************************************************************* Service Timeout functions ******************************************/
	function emailServiceTimeOut(tooltip,submitBtn,parentForm){
		if(emailcallstatus == true)
		{
			myElementArray[tooltip.tooltip('option').tip] = 0;
			submitBtn.removeAttr('disabled');
			$(parentForm).removeAttr('state');
			tClassName = "."+tooltip.tooltip('option').tip;
			$( tClassName).tooltip( "disable" );
			removeDisableFormButtons();
		}
	}
	
	function phoneServiceTimeOut(tooltip,submitBtn,parentForm){
		if(phonecallstatus == true)
		{
			myElementArray[tooltip.tooltip('option').tip] = 0;
			submitBtn.removeAttr('disabled');
			$(parentForm).removeAttr('state');
			tClassName = "."+tooltip.tooltip('option').tip;
			$( tClassName).tooltip( "disable" );
			removeDisableFormButtons();
		}
	}
	
	function nameServiceTimeOut(tooltip,submitBtn,fieldElements){
		if(namecallstatus == true)
		{
			myElementArray[tooltip.tooltip('option').tip] = 0;
			submitBtn.removeAttr('disabled');
			removeDisableFormButtons();
			var parentForm = fieldElements.form;
			$(parentForm).removeAttr('state');
			tClassName = "."+tooltip.tooltip('option').tip;
			$( tClassName).tooltip( "disable" );
		}
	}
		
		
	function ipServiceTimeOut(tooltip,submitBtn,parentForm){
		
		if(ipcallstatus == true)
		{
				myElementArray[tooltip.tooltip('option').tip] = 0;
			submitBtn.removeAttr('disabled');
			removeDisableFormButtons();
			$(parentForm).removeAttr('state');
			tClassName = "."+tooltip.tooltip('option').tip;
			$( tClassName).tooltip( "disable" );
		}
	}
	
	function addressServiceTimeOut(tooltip,submitBtn,fieldElements){	
		if(addresscallstatus  == true)
		{
				myElementArray[tooltip.tooltip('option').tip] = 0;
			submitBtn.removeAttr('disabled');
			removeDisableFormButtons();
			var parentForm = fieldElements.form;
			$(parentForm).removeAttr('state');
			tClassName = "."+tooltip.tooltip('option').tip;
			$( tClassName).tooltip( "disable" );
		}
	}
	/************************************************************* Service Timeout functions ******************************************/
	
	/************************************************************* Service Request functions ******************************************/
	function emailServiceRequest(email,tooltip,button,parentForm,field,autocorrect) {
		var url = serverURL + "emails/process/?type=json&apikey="+ apiKey + "&domain="+ domainname +"&v1="+ affiliateid +"&v2="+ subaffiliateid + "&";
		
		url = url + "captcha_status=" + service_captcha["email"] + "&"	+ "bypass_status=" + bypass_email + "&"	
		extrastring = getPostBackData(parentForm);
		if(extrastring != '')
		{
			url = url + extrastring;
		}
		if(globalSubmitType == 'onSubmit')
		{
			extrastring = getESPData(parentForm);
			if(extrastring != '')
			{
				url = url + extrastring;
			}
		}
		url = url + "callback=?";
		if(autocorrect == undefined)
			autocorrect = 0;
		else
			autocorrect = 1;
		emailcallstatus = true;
		emailtimeOut = setTimeout(function() {emailServiceTimeOut(tooltip,button,parentForm);}, emailtimeout); 
		$.getJSON(
				  url, {"email" : email,"autocorrect":autocorrect},
				  function(json){
					emailcallstatus = false;  
					clearTimeout (emailtimeOut);
					var service_email = json["email"];	
					emailSuccessResponseHandler(service_email,tooltip,button,parentForm,field);
		});
	}
	
	
	function phoneServiceRequest(phone,tooltip,button,phoneType,parentForm,field) {
		var url = serverURL + "phone/process/?type=json&apikey="+ apiKey + "&domain="+ domainname + "&v1="+ affiliateid +"&v2="+ subaffiliateid + "&phonetype="+ phoneType +"&";
		url = url + "captcha_status=" + service_captcha["phone"] + "&"
		extrastring = getPostBackData(parentForm);
		if(extrastring != '')
		{
			url = url + extrastring;
		}
		url = url + "callback=?";
		
		phonecallstatus = true;
		phonetimeOut = setTimeout(function() {phoneServiceTimeOut(tooltip,button,parentForm);}, phonetimeout); 
		$.getJSON(
				  url, {"phone" : phone},
				  function(json){
					phonecallstatus = false;  
					clearTimeout (phonetimeOut);
					var service_phone = json["phone"];
					phoneSuccessResponseHandler(service_phone,tooltip,button,parentForm,field);
		});
	}
	
	function addressVerifyServiceRequest(address,tooltip,button,fieldElements) {
		var url = serverURL + "address/process/?type=json&apikey="+ apiKey + "&domain="+ domainname + "&v1="+ affiliateid +"&v2="+ subaffiliateid + "&";
		url = url + "captcha_status=" + service_captcha["address"] + "&"
		extrastring = getPostBackData(fieldElements.form);
		if(extrastring != '')
		{
			url = url + extrastring;
		}
		url = url + "callback=?";
		
		addresscallstatus = true;
		addresstimeOut = setTimeout(function() {addressServiceTimeOut(tooltip,button,fieldElements);}, addresstimeout); 
		var address_param = {"street" : address.street, "zip" : address.zip, "city" : address.city, "state" : address.state};
		$.getJSON(
				   url, address_param, 
				  function(json){
					addresscallstatus = false;  
					clearTimeout (addresstimeOut);
					var service_address = json["address"];	
					addressSuccessResponseHandler(service_address,tooltip,button,fieldElements);
		});
	}
	
	function ipVerifyServiceRequest(ip,tooltip,button,parentForm,field) {
		var url = serverURL + "ipdata/process/?type=json&apikey="+ apiKey + "&domain="+ domainname + "&v1="+ affiliateid +"&v2="+ subaffiliateid +"&";
		url = url + "captcha_status=" + service_captcha["ip"] + "&"		
		extrastring = getPostBackData(parentForm);
		if(extrastring != '')
		{
			url = url + extrastring;
		}
		url = url + "callback=?";
		ipcallstatus = true;
		iptimeOut = setTimeout(function() {ipServiceTimeOut(tooltip,button,parentForm);}, iptimeout); 
		$.getJSON(
				   url, {"ip" : ip}, 
				  function(json){
					ipcallstatus = false;  
					clearTimeout (iptimeOut);
					var service_ip = json["ipdata"];	
					ipSuccessResponseHandler(service_ip,tooltip,button,parentForm,field);
		});
	}
	
	function nameVerifyServiceRequest(names,tooltip,button,fieldElements) {
		var url = serverURL + "name/process/?type=json&apikey="+ apiKey + "&domain="+ domainname + "&v1="+ affiliateid +"&v2="+ subaffiliateid + "&";
		url = url + "captcha_status=" + service_captcha["name"] + "&"		
		extrastring = getPostBackData(fieldElements.form);
		if(extrastring != '')
		{
			url = url + extrastring;
		}
		url = url + "callback=?";
		
		namecallstatus = true;
		nametimeOut = setTimeout(function() {nameServiceTimeOut(tooltip,button,fieldElements);}, nametimeout); 
		var name_param = {"firstname" : names.firstname, "lastname" : names.lastname};
		$.getJSON(
				   url, name_param, 
				  function(json){
					namecallstatus = false;  
					clearTimeout (nametimeOut);
					var service_name = json["name"];	
					nameSuccessResponseHandler(service_name,tooltip,button,fieldElements);
		});
	}
	
	/************************************************************* Service Request functions ******************************************/
	
	/************************************************************* Service Success Response Handler functions ******************************************/
	
	function emailSuccessResponseHandler(ajaxResponse,tooltip,submitBtn,parentForm,field ){
		var tClassName = "."+tooltip.tooltip('option').tip;
		var tip_pos = '';
		tip_pos = field.attr('tip_pos');
		if(tip_pos == undefined) tip_pos = '';
		if(tip_pos != '') tip_pos = tip_pos + '-';
		if(ajaxResponse.error){
			$( tClassName).tooltip( "disable" );
			$( tClassName).tooltip( "enable" );
			
			if(ajaxResponse.status == 'correction')
			{
				$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_warning_class});
				$(tClassName).tooltip({content:''});
				$(tClassName).tooltip().off('focusout mouseleave');
				user_email = ajaxResponse.user_email;
				correct_email = ajaxResponse.correct_email;
				xverifySuggestEmail(user_email,correct_email,tooltip,submitBtn,parentForm,field);
				return;
			}
			if(ajaxResponse.status == 'bypass')
			{
				$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_warning_class});
				$(tClassName).tooltip({content:''});
				$(tClassName).tooltip().off('focusout mouseleave');
				
				user_email = ajaxResponse.user_email;
				bypass_message = ajaxResponse.message;
				valid_message = ajaxResponse.message_valid;
				inavlid_message = ajaxResponse.message_invalid;
				
				call_function = '';
				if(ajaxResponse.call_function != undefined && $.trim(ajaxResponse.call_function) != '' )
					call_function = ajaxResponse.call_function;
				
				if(bypass_message == '')
				{
					service_captcha["email"] = 0;
					myElementArray[tooltip.tooltip('option').tip] = 0;
					$(parentForm).removeAttr('state');
					submitBtn.removeAttr('disabled');	
					removeDisableFormButtons();	
					field.unbind('change');
					$( tClassName).tooltip( "disable" );
					return;
				}
				xverifyByPassEmail(user_email,tooltip,submitBtn,parentForm,field,bypass_message,valid_message,inavlid_message,call_function);
				return;
			}
			
			
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			if(is_mobile == true){
				$(tClassName).tooltip({content:'Invalid'});
				field.css({'border':'2px solid #F00'});
			}
			else
			{
				$(tClassName).tooltip({content:ajaxResponse.message});
			}
			$(tClassName).tooltip().off('focusout mouseleave');
			
			if(ajaxResponse.status == 'pending')
			{
				showRecaptcha(showCaptchaDiv,reCaptchaKey,'email',field);
			}
			else
			{
				service_captcha["email"] = 0;
			}
			if(formautosubmit == true)
			{
				submitBtn.removeAttr('disabled');
				removeDisableFormButtons();
			}
		}
		else
		{
			service_captcha["email"] = 0;
			myElementArray[tooltip.tooltip('option').tip] = 0;
			$(parentForm).removeAttr('state');
			submitBtn.removeAttr('disabled');
			removeDisableFormButtons();		
			$( tClassName).tooltip( "disable" );
			field.val(ajaxResponse.address);
			verify_email_submit = true;
			field.attr('xvverify','1');
			field.attr('xvverify_value',ajaxResponse.address);
			verified_email = ajaxResponse.address;
			verified_email_elment = field;
			form_count_status = $(parentForm).attr('form_count');
			if(typeof form_count_status  === 'undefined' )
			{
			}
			else
			{
				verified_email_elment.unbind('change', emailVerifiedChangeHandler);
				verified_email_elment.bind('change', emailVerifiedChangeHandler);
			}
		}
		if(ajaxResponse.call_function != undefined && $.trim(ajaxResponse.call_function) != '' )
		{
			functionstatus  = $.isFunction(window[ajaxResponse.call_function]);
			if(functionstatus)
			{
				var callbackfunction_args = []
				callbackfunction_args.status = ajaxResponse.status;
				callbackfunction_args.responsecode = ajaxResponse.responsecode;
				callbackfunction_args.message = ajaxResponse.message;
				callbackfunction_args.email = ajaxResponse.address;
				window[ajaxResponse.call_function](callbackfunction_args);
			}
		}
		formButtonCheckHandler(tooltip,submitBtn,parentForm);
		//field.removeAttr('disabled');
	}
	
	function phoneSuccessResponseHandler(ajaxResponse,tooltip,submitBtn,parentForm,field){
		tClassName = "."+tooltip.tooltip('option').tip;
		tip_pos = field.attr('tip_pos');
		if(tip_pos == undefined) tip_pos = '';
		if(tip_pos != '') tip_pos = tip_pos + '-';
		var tip_pos = '';
		if(ajaxResponse.error){
			$( tClassName).tooltip( "disable" );
			$( tClassName).tooltip( "enable" );
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
			}
			if(ajaxResponse.status == 'pending')
			{
				showRecaptcha(showCaptchaDiv,reCaptchaKey,'phone',field);
			}
			if(ajaxResponse.status == 'bypass')
			{
				$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_warning_class});
				$(tClassName).tooltip({content:''});
				$(tClassName).tooltip().off('focusout mouseleave');
			
				bypass_message = ajaxResponse.message;
				message_valid =  ajaxResponse.message_valid;
				message_invalid =  ajaxResponse.message_invalid;
							
				call_function = '';
				if(ajaxResponse.call_function != undefined && $.trim(ajaxResponse.call_function) != '' )
					call_function = ajaxResponse.call_function;
				
				if(bypass_message == '')
				{
					field.unbind('change');
					service_captcha["phone"] = 0;
					myElementArray[tooltip.tooltip('option').tip] = 0;
					$(parentForm).removeAttr('state');
					submitBtn.removeAttr('disabled');
					removeDisableFormButtons();	
					$( tClassName).tooltip( "disable" );
					return;
				}			
				xverifyByPassPhone(tooltip,bypass_message,message_valid,message_invalid,submitBtn,parentForm,call_function);
				return;
			}
			else
			{
				service_captcha["phone"] = 0;
			}
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			$(tClassName).tooltip({content:ajaxResponse.message});
			$(tClassName).tooltip().off('focusout mouseleave');
			
			if(formautosubmit == true)
			{
				submitBtn.removeAttr('disabled');
				removeDisableFormButtons();
			}
		}
		else
		{
			service_captcha["phone"] = 0;
			myElementArray[tooltip.tooltip('option').tip] = 0;
			submitBtn.removeAttr('disabled');
			removeDisableFormButtons();
			$(parentForm).removeAttr('state');
			$( tClassName).tooltip( "disable" );
			verify_phone_submit = true;
			field.attr('xvverify','1');
			field.attr('xvverify_value',ajaxResponse.value);
		}
		if(ajaxResponse.call_function != undefined && $.trim(ajaxResponse.call_function) != '' )
		{
			functionstatus  = $.isFunction(window[ajaxResponse.call_function]);
			if(functionstatus)
			{
				var callbackfunction_args = []
				callbackfunction_args.status = ajaxResponse.status;
				callbackfunction_args.message = ajaxResponse.message;
				window[ajaxResponse.call_function](callbackfunction_args);
			}
		}
		formButtonCheckHandler(tooltip,submitBtn,parentForm);
	}
	
	function ipSuccessResponseHandler(ajaxResponse,tooltip,submitBtn,parentForm,field ){
		var tip_pos = '';
		tClassName = "."+tooltip.tooltip('option').tip;
		if(ajaxResponse.error){
			$( tClassName).tooltip( "disable" );
			$( tClassName).tooltip( "enable" );
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			$(tClassName).tooltip({content:ajaxResponse.message});
			$(tClassName).tooltip().off('focusout mouseleave');
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
			}
			if(ajaxResponse.status == 'pending')
			{
				showRecaptcha(showCaptchaDiv,reCaptchaKey,'ip',field);
			}
			else
			{
				service_captcha["ip"] = 0;
			}
			if(formautosubmit == true)
			{
				submitBtn.removeAttr('disabled');
				removeDisableFormButtons();
			}
		}
		else
		{
			service_captcha["ip"] = 0;
			myElementArray[tooltip.tooltip('option').tip] = 0;
			submitBtn.removeAttr('disabled');
			removeDisableFormButtons();
			$(parentForm).removeAttr('state');
			$( tClassName).tooltip( "disable" );
		}
		formButtonCheckHandler(tooltip,submitBtn,parentForm);
	}
	
	function nameSuccessResponseHandler(ajaxResponse,tooltip,submitBtn,fieldElements ){
		var tip_pos = '';
		tClassName = "."+tooltip.tooltip('option').tip;
		if(ajaxResponse.error){
			$( tClassName).tooltip( "disable" );
			$( tClassName).tooltip( "enable" );
			$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
			$(tClassName).tooltip({content:ajaxResponse.message});
			if(is_mobile == true){
				field.css({'border':'2px solid #F00'});
			}
			var firstNameElement = fieldElements.firstname;
			if(is_mobile == true){
				firstNameElement.css({'border':'2px solid #F00'});
			}
			if(ajaxResponse.status == 'pending')
			{
				showRecaptcha(showCaptchaDiv,reCaptchaKey,'name',firstNameElement);
			}
			else
			{
				service_captcha["name"] = 0;
			}
			$(tClassName).tooltip().off('focusout mouseleave');
			
			if(formautosubmit == true)
			{
				submitBtn.removeAttr('disabled');
				removeDisableFormButtons();
			}
		}
		else
		{
			service_captcha["name"] = 0;
			myElementArray[tooltip.tooltip('option').tip] = 0;
			var firstNameElement = fieldElements.firstname;
			var lastNameElement = fieldElements.lastname;
			var parentForm = fieldElements.form;
			firstNameElement.val(ajaxResponse.first);
			lastNameElement.val(ajaxResponse.last);
			submitBtn.removeAttr('disabled');
			removeDisableFormButtons();
			$(parentForm).removeAttr('state');
			tClassName = "."+tooltip.tooltip('option').tip;
			$( tClassName).tooltip( "disable" );
			firstNameElement.attr('xvverify','1');
		}
		formButtonCheckHandler(tooltip,submitBtn,parentForm);
	}
	
	function addressSuccessResponseHandler(ajaxResponse,tooltip,submitBtn,fieldElements ){
		tClassName = "."+tooltip.tooltip('option').tip;
		var tip_pos = '';
		if(ajaxResponse.error){
			var streetElement = fieldElements.street;
			tip_pos = streetElement.attr('tip_pos');
			if(tip_pos == undefined) tip_pos = '';
			if(tip_pos != '') tip_pos = tip_pos + '-';
		
			var zipElement = fieldElements.zip;
			if(tClassName == '.undefined')
			{
				tClassName = "."+streetElement.attr('use_class');
			}
			tZipClassName = "."+zipElement.attr('use_class');
			ziptooltip = zipElement.tooltip();
			streettooltip = streetElement.tooltip();
			$( tZipClassName).tooltip( "disable" );
			
			if(is_mobile == true)
			{
				streetElement.css({'border':'2px solid #F00'});
			}
			if(ajaxResponse.status == 'pending')
			{
				showRecaptcha(showCaptchaDiv,reCaptchaKey,'address',streetElement);
			}
			
			if(ajaxResponse.status == 'bypass')
			{
				$( tClassName).tooltip( "disable" );
				$( tClassName).tooltip( "enable" );
				$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_warning_class});
				$(tClassName).tooltip({content:''});
				$(tClassName).tooltip().off('focusout mouseleave');
			
				bypass_message = ajaxResponse.message;
				valid_message = ajaxResponse.message_valid;
				inavlid_message = ajaxResponse.message_invalid;
				
				call_function = '';
				if(ajaxResponse.call_function != undefined && $.trim(ajaxResponse.call_function) != '' )
					call_function = ajaxResponse.call_function;
				
				if(bypass_message == '')
				{
					var streetElement = fieldElements.street;
					var zipElement = fieldElements.zip;
					var cityElement = fieldElements.city;
					var stateElement = fieldElements.state;
					
					if(streetElement.length > 0) {streetElement.unbind('change')};
					if(zipElement.length > 0) {zipElement.unbind('change')};
					if(cityElement.length > 0) {cityElement.unbind('change')};
					if(stateElement.length > 0) {stateElement.unbind('change')};
					
					myElementArray[tooltip.tooltip('option').tip] = 0;
					parentForm = fieldElements.form;
					service_captcha["address"] = 0;
					$(parentForm).removeAttr('state');
					submitBtn.removeAttr('disabled');	
					removeDisableFormButtons();
					$( tClassName).tooltip( "disable" );
					return;
				}	
				xverifyByPassAddress(tooltip,bypass_message,valid_message,inavlid_message,submitBtn,fieldElements,call_function);
				return;
			}
			else
			{
				service_captcha["address"] = 0;
			}
			
			$(tClassName).tooltip( "disable");
			$(tZipClassName).tooltip( "disable");
			var gotErrorMsg = ajaxResponse.message;
			gotErrorMsg = gotErrorMsg.toLowerCase();
			if(gotErrorMsg.indexOf('zip') <= -1)
			{
				$(tClassName).tooltip( "enable" );
				$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
				$(tClassName).tooltip({content:ajaxResponse.message});
				$(tClassName).tooltip().off('focusout mouseleave');
			}
			else
			{
				$(tZipClassName).tooltip( "enable" );
				$(tZipClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_error_class});
				$(tZipClassName).tooltip({content:ajaxResponse.message});
				$(tZipClassName).tooltip( "open" );
				$(tZipClassName).tooltip().off('focusout mouseleave');
			}
			
			if(formautosubmit == true)
			{
				submitBtn.removeAttr('disabled');
				removeDisableFormButtons();
			}
		}
		else
		{
			myElementArray[tooltip.tooltip('option').tip] = 0;
			var streetElement = fieldElements.street;
			var zipElement = fieldElements.zip;
			var cityElement = fieldElements.city;
			var stateElement = fieldElements.state;
			var parentForm = fieldElements.form;
			var street_2 = fieldElements.street_2;
			
			tip_pos = streetElement.attr('tip_pos');
			if(tip_pos == undefined) tip_pos = '';
			if(tip_pos != '') tip_pos = tip_pos + '-';
			
			if(tClassName == '.undefined')
			{
				tClassName = "."+streetElement.attr('use_class');
			}
			tZipClassName = "."+zipElement.attr('use_class');
			
			service_captcha["address"] = 0;
			verify_address_submit = true;
			if(cityElement.length > 0) cityElement.val(ajaxResponse.city);
			if(stateElement.length > 0) stateElement.val(ajaxResponse.state);
			
			if(street_2.length > 0)
			{
			}
			else
			{
				streetElement.val($.trim(ajaxResponse.street));
			}
			zipElement.val(ajaxResponse.zip);
			
			submitBtn.removeAttr('disabled');
			removeDisableFormButtons();
			$(parentForm).removeAttr('state');
			streetElement.attr('xvverify','1');
			streetElement.attr('xvverify_value',ajaxResponse.street);
			zipElement.attr('xvverify_value',ajaxResponse.zip);
			if(ajaxResponse.response_code != 9)
			{
				$( tClassName).tooltip( "disable" );
			}
			else
			{
				$( tClassName).tooltip( "disable" );
				if(street_2.length > 0 && ajaxResponse.response_code == 9)
				{
					street2tooltip = street_2.tooltip();
					tStreet2ClassName = "."+street2tooltip.tooltip('option').tip;
					tClassName = tStreet2ClassName;
				}
				if(formautosubmit == true)
				{
					$( tClassName).tooltip( "disable" );
				}
				else
				{
					$( tClassName).tooltip( "disable" );
					$( tClassName).tooltip( "enable" );
					$(tClassName).tooltip({ "ui-tooltip":tip_pos+tooltip_warning_class});
					$(tClassName).tooltip({content:ajaxResponse.message});
					if(is_mobile == true){
						field.css({'border':'2px solid #F00'});
					}
					if(street_2.length > 0 && ajaxResponse.response_code == 9)
					{
						$(tClassName).tooltip( "open" );
					}
					$(tClassName).tooltip().off('focusout mouseleave');
				}
				
			}
		}
		if(ajaxResponse.call_function != undefined && $.trim(ajaxResponse.call_function) != '' )
		{
			functionstatus  = $.isFunction(window[ajaxResponse.call_function]);
			if(functionstatus)
			{
				var callbackfunction_args = []
				callbackfunction_args.status = ajaxResponse.status;
				callbackfunction_args.message = ajaxResponse.message;
				window[ajaxResponse.call_function](callbackfunction_args);
			}
		}
		formButtonCheckHandler(tooltip,submitBtn,parentForm);
	}
	
	/************************************************************* Service Success Response Handler functions ******************************************/
	
	
	/************************************************************* Service Response Handler functions ******************************************/
	
	/************************************************************* Service Response Handler functions ******************************************/
	
	function getPostBackData(form)
	{
		var returnString = '';
		fieldclass = 'xverify_postback';
		fieldElement = getInputElementsByAttributeFromSpecficForms('class',fieldclass,form)
		for (var i = 0; i < fieldElement.length; i++)
		{
			field = $(fieldElement[i]);
			name = field.attr('name');	
			value = field.attr('value');
			returnString = returnString + 'postback[' +  	name + ']=' + value + '&';
		}
		return returnString;
	}
	
	function getESPData(form)
	{
		var esp_name = '';
		var esp_list_id = '';
		var esp_details = {};
		if(elements_field_esp_name != '')
		{
			esp_name = $('#'+elements_field_esp_name).val();
			if(esp_name == '' || esp_name == false)
			{
				esp_name = $("input[name="+elements_field_esp_name+"]").val();
			}
			if(esp_name == '' || esp_name == false)
			{
				esp_name = $('.'+elements_field_esp_name).val();
			}
		}
		
		if(esp_name != '' && elements_field_list_id != '')
		{
			esp_list_id = $('#'+elements_field_list_id).val();
			if(esp_list_id == '' || esp_list_id == false)
			{
				esp_list_id = $("input[name="+elements_field_list_id+"]").val();
			}
			if(esp_list_id == '' || esp_list_id == false)
			{
				esp_list_id = $('.'+elements_field_list_id).val();
			}
		}
		
		if(esp_name != '' && esp_list_id != '' && $.isPlainObject(elements_field_esp_contacts))
		{
			for ( var index in elements_field_esp_contacts) {
				elment_name = elements_field_esp_contacts[index];
				elment_value = '';
				elment_value = $('#'+elment_name).val();
				if(elment_value == '' || elment_value == false)
				{
					elment_value = $("input[name="+elment_name+"]").val();
				}
				if(elment_value == '' || elment_value == false)
				{
					elment_value = $('.'+elment_name).val();
				}
				
				if(elment_value != '')
				{
					esp_details[index] = elment_value;
				}
			}
		}
		var returnString = '';
		
		if(esp_name != '' && esp_list_id != '')
		{
			returnString = returnString + 'esps[javascript]=1&';
			returnString = returnString + 'esps[espname]=' + esp_name + '&';
			returnString = returnString + 'esps[listid]=' + esp_list_id + '&';
		}
		if($.isPlainObject(esp_details))
		{
			for ( var index in esp_details){
				field_value = esp_details[index];
				returnString = returnString + 'esps[userdata]['+index+']=' + field_value + '&';
			}
		}
		return returnString;
	}
	function removeXverifyServiceFromElement(selector)
	{
		var inputElements = $(selector); 
		inputElements.attr('service','0');
		inputElements.trigger('change'); 
		var tooltip = inputElements.tooltip();
		tClassName = "."+tooltip.tooltip('option').tip;
		$( tClassName).tooltip( "disable" );
		inputElements.unbind('change');
		inputElements.unbind('focus');
	}
	function addXverifyServiceOnElement(selector){
		if($(this).xvServiceExist('email'))
		{
			var inputElements = $(selector); 
			bindToolTipOnInputElements(inputElements,{ message_position : tooltip_position });
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			var tooltip = inputElements.tooltip();
			tClassName = "."+tooltip.tooltip('option').tip;
			$( tClassName).tooltip( "disable" );
			$( tClassName).tooltip( "enable" );
			$(tClassName).tooltip({content: ""});
			inputElements.attr('service','1');
		}
	}
	
	function bindUserFieldsByIdOrNameOrClass(service_name,formname){
		if(service_name == 'email' && $(this).xvServiceExist(service_name))
		{
			email_tooltip_position = getTooltipPosition(elements_field_email.tooltip_position);
			var inputElements = getInputElementsByAttributeFromSpecficForms("id", elements_field_email.id,formname);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position,tip_pos:elements_field_email.tooltip_position });
			inputElements.bind('change', emailChangeHandler);
			inputElements.bind('xv_submit_form', emailChangeHandler);
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromSpecficForms("name", elements_field_email.name,formname);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position,tip_pos:elements_field_email.tooltip_position });
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			
			inputElements.unbind('xv_submit_form', emailChangeHandler);
			inputElements.bind('xv_submit_form', emailChangeHandler);
			
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromSpecficForms("class", elements_field_email.class,formname);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position,tip_pos:elements_field_email.tooltip_position });
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			
			inputElements.unbind('xv_submit_form', emailChangeHandler);
			inputElements.bind('xv_submit_form', emailChangeHandler);
			
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromSpecficFormsFieldType("id", elements_field_email.id,'email',formname);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position,tip_pos:elements_field_email.tooltip_position });
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			
			inputElements.unbind('xv_submit_form', emailChangeHandler);
			inputElements.bind('xv_submit_form', emailChangeHandler);
			
			inputElements.unbind('xv_submit_form', emailChangeHandler);
			inputElements.bind('xv_submit_form', emailChangeHandler);
			
			if(inputElements.length > 0) return inputElements;
			inputElements = getInputElementsByAttributeFromSpecficFormsFieldType("name", elements_field_email.name,'email',formname);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position,tip_pos:elements_field_email.tooltip_position });
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			
			inputElements.unbind('xv_submit_form', emailChangeHandler);
			inputElements.bind('xv_submit_form', emailChangeHandler);
			
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromSpecficFormsFieldType("class", elements_field_email.name,'email',formname);
			bindToolTipOnInputElements(inputElements,{ message_position : email_tooltip_position,tip_pos:elements_field_email.tooltip_position });
			inputElements.unbind('change', emailChangeHandler);
			inputElements.bind('change', emailChangeHandler);
			
			inputElements.unbind('xv_submit_form', emailChangeHandler);
			inputElements.bind('xv_submit_form', emailChangeHandler);
			
			return inputElements;
		}
		else if(service_name == 'phone' && $(this).xvServiceExist(service_name))
		{
			phone_tooltip_position = getTooltipPosition(elements_field_phone.tooltip_position);
			var inputElements = getInputElementsByAttributeFromSpecficForms("id", elements_field_phone.id,formname);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position  ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.bind('change',phoneChangeHandler);
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromSpecficForms("name", elements_field_phone.name,formname);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.unbind('change', phoneChangeHandler);
			inputElements.bind('change', phoneChangeHandler);
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromSpecficFormsFieldType("id", elements_field_phone.id,'tel',formname);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.unbind('change', phoneChangeHandler);
			inputElements.bind('change', phoneChangeHandler);
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromSpecficFormsFieldType("name", elements_field_phone.name,'tel',formname);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.unbind('change', phoneChangeHandler);
			inputElements.bind('change', phoneChangeHandler);
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromSpecficForms("class", elements_field_phone.class,formname);
			bindToolTipOnInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position});
			inputElements.unbind('change', phoneChangeHandler);
			inputElements.bind('change', phoneChangeHandler);
			if(inputElements.length > 0) return inputElements;
			
			inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_phone_1");
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_phone_1");
			inputElements.unbind('change', multiPhoneOtherFieldChangeHandler);
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_phone_2");
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_phone_2");
			inputElements.unbind('change', multiPhoneOtherFieldChangeHandler);
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("name", "multifield_phone_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position},multiPhoneToolTipBeforeShowHandler);
			inputElements.bind('change', multiPhoneChangeHandler);
	
			inputElements = getInputElementsByAttributeFromAllForms("id", "multifield_phone_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position},multiPhoneToolTipBeforeShowHandler);
			inputElements.unbind('change', multiPhoneChangeHandler);
			inputElements.bind('change', multiPhoneChangeHandler);
			if(inputElements.length > 0) return inputElements;
			
			
			inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_phone_1");
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_phone_2");
			inputElements.bind('change', multiPhoneOtherFieldChangeHandler);
			
			inputElements = getInputElementsByAttributeFromAllForms("class", "xverify_multifield_phone_3");
			bindToolTipOnMultiInputElements(inputElements,{ message_position : phone_tooltip_position ,tip_pos:elements_field_phone.tooltip_position},multiPhoneToolTipBeforeShowHandler);
			inputElements.unbind('change', multiPhoneChangeHandler);
			inputElements.bind('change', multiPhoneChangeHandler);
			return inputElements;
		}
		else if(service_name == 'address' && $(this).xvServiceExist(service_name))
		{
			address_tooltip_position = getTooltipPosition(elements_field_street.tooltip_position);
			var inputElements = getInputElementsByAttributeFromSpecficForms("id", elements_field_street.id,formname);
			bindToolTipOnInputElements(inputElements,{ message_position : address_tooltip_position ,tip_pos:elements_field_street.tooltip_position});
			inputElements.bind('change', streetChangeHandler);
			
			if(inputElements.length == 0)
			{
				inputElements = getInputElementsByAttributeFromSpecficForms("name", elements_field_street.id,formname);
				bindToolTipOnInputElements(inputElements,{ message_position : address_tooltip_position ,tip_pos:elements_field_street.tooltip_position});
				inputElements.unbind('change', streetChangeHandler);
				inputElements.bind('change', streetChangeHandler);
			}
				//if(inputElements.length > 0) return inputElements;
			if(inputElements.length == 0)
			{
				inputElements = getInputElementsByAttributeFromSpecficForms("class", elements_field_street.class,formname);
				bindToolTipOnInputElements(inputElements,{ message_position : address_tooltip_position ,tip_pos:elements_field_street.tooltip_position});
				inputElements.unbind('change', streetChangeHandler);
				inputElements.bind('change', streetChangeHandler);
			}
			//if(inputElements.length > 0) return inputElements;
			
			var inputElements_1 = getInputElementsByAttributeFromSpecficForms("name",elements_field_zip.name,formname);
			bindToolTipOnInputElements(inputElements_1,{ message_position : address_tooltip_position ,tip_pos:elements_field_street.tooltip_position});
			inputElements_1.bind('change', addressOtherFieldChangeHandler);
			
			if(inputElements_1.length == 0)
			{
				inputElements_1 = getInputElementsByAttributeFromSpecficForms("id", elements_field_zip.id,formname);
				bindToolTipOnInputElements(inputElements_1,{ message_position : address_tooltip_position ,tip_pos:elements_field_street.tooltip_position});
				inputElements_1.unbind('change', addressOtherFieldChangeHandler);
				inputElements_1.bind('change', addressOtherFieldChangeHandler);
			}
			
			if(inputElements_1.length == 0)
			{
				inputElements_1 = getInputElementsByAttributeFromSpecficForms("class", elements_field_zip.class,formname);
				bindToolTipOnInputElements(inputElements_1,{ message_position : address_tooltip_position ,tip_pos:elements_field_street.tooltip_position});
				inputElements_1.unbind('change', addressOtherFieldChangeHandler);
				inputElements_1.bind('change', addressOtherFieldChangeHandler);
			}
			
			var inputElements_2;
			if(inputElements_1.length > 0) inputElements_2 = inputElements.add(inputElements_1);
			
			address_obj = {'trigger_data':inputElements_1,'bind_data':inputElements_2}
			return address_obj;
		}
		else
		{
			return false;
		}
	}
	
	function checkValidateTimeout(formname,service_name)
	{
		if(service_name == 'email')
		{
			if(emailcallstatus == true) return;
		}
		else if(service_name == 'phone')
		{
			if(phonecallstatus == true) return;
		}
		else if(service_name == 'address')
		{
			if(addresscallstatus == true) return;
		}
		
		var parentForm = formname;
		var submitBtn = $(parentForm).find("input[type='submit']");
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("input[type='image']");
		}
		if(submitBtn.length == 0)
		{
			submitBtn = $(parentForm).find("button[type='submit']");
		}
		submitBtn.removeAttr('disabled');
		removeDisableFormButtons();
	}
	function xvFormSubmitHandler(event){
		var formHandler = event.currentTarget
		form_state = $(formHandler).attr('state');
		
		if(form_state == 'ok')
		{
			if(verified_email_elment.val() != verified_email)
			{
				form_state = '';
				$(formHandler).attr('state',form_state);
			}
		}
			
		if(form_state != 'ok')
		{
			$.bindXVServicesOnInputFields(formHandler);
			//event.preventDefault();
			event.stopImmediatePropagation();
			return false;
		}
		
		form_count_status = $(formHandler).attr('form_count');
		if(typeof form_count_status  === 'undefined' )
		{
		}
		else
		{
			form_data = forms[form_count_status];
			$(formHandler).attr('onsubmit',form_data.fun_name);
		}
		return true;
	}
	function emailVerifiedChangeHandler(event)
	{
		var field = $(event.currentTarget);
		var parentForm = $(field).closest('form');
		onsubmit_event = parentForm.attr('onsubmit');
		if(typeof onsubmit_event  === 'undefined' )
		{
		}
		else
		{
			onsubmitbind = true;
			onsubmitstring = onsubmit_event;
			parentForm.removeAttr('onsubmit');
			parentForm.attr('form_count',form_count);
			var form_data = []
			form_data.fun_status = onsubmitbind;
			form_data.fun_name = onsubmitstring;
			forms[form_count] = form_data;
			form_count++;
		}
		field.unbind('change', emailVerifiedChangeHandler);
	}
	function initalizeFormButton(button_elment)
	{
		var field = $('#'+button_elment);
		if(field.length > 0)
		{
			elements_field_button = field;
			return;
		}
		
		field = $('.'+button_elment);
		if(field.length > 0)
		{
			elements_field_button = field;
			return;
		}
		
		field = $( "button[name$='"+button_elment+"']" );
		if(field.length > 0)
		{
			elements_field_button = field;
			return;
		}
		//elements_field_button
	}
	
	function addDisableFormButtons(submitBtn)
	{
		if(elements_field_button.length > 0)
		{
			if(typeof submitBtn !== 'undefined')
			{
				if(submitBtn.length > 1)
				{
					submitBtn.removeAttr('disabled');
				}
			}
			elements_field_button.attr('disabled', 'true');
		}
	}
	function removeDisableFormButtons()
	{
		if(elements_field_button.length > 0)
			elements_field_button.removeAttr('disabled');
	}
	function findElmentInPage(elmentName)
	{
		var elemntData = $('#'+elmentName);
		if(elemntData.length > 0)
			return elmentName;
			
		elemntData = $('.'+elmentName);	
		if(elemntData.length > 0)
			return elmentName;
		
		elemntData = $( "input[name*='"+elmentName+"']" )	;
		if(elemntData.length > 0)
			return elmentName;
		return '';	
			
	}
	/**
	 * Get the closest matching element up the DOM tree.
	 * @private
	 * @param  {Element} elem     Starting element
	 * @param  {String}  selector Selector to match against
	 * @return {Boolean|Element}  Returns null if not match found
	 */
	$.fn.getClosest = function ( elem, selector ) {

		// Element.matches() polyfill
		if (!Element.prototype.matches) {
			Element.prototype.matches =
				Element.prototype.matchesSelector ||
				Element.prototype.mozMatchesSelector ||
				Element.prototype.msMatchesSelector ||
				Element.prototype.oMatchesSelector ||
				Element.prototype.webkitMatchesSelector ||
				function(s) {
					var matches = (this.document || this.ownerDocument).querySelectorAll(s),
						i = matches.length;
					while (--i >= 0 && matches.item(i) !== this) {}
					return i > -1;
				};
		}

		// Get closest match
		for ( ; elem && elem !== document; elem = elem.parentNode ) {
			if ( elem.matches( selector ) ) return elem;
		}

		return null;

	};
	$.fn.xvBindFirst = function(name, fn) {
	  var elem, handlers, i, _len;
	  this.bind(name, fn);
	  for (i = 0, _len = this.length; i < _len; i++) {
		elem = this[i];
		handlers = $._data(elem).events[name.split('.')[0]];
		handlers.unshift(handlers.pop());
	  }
	};
	$.extend({
			bindServicesOnInputFields : function(formsubmit,inputelement)
			{
				if(typeof formsubmit === 'undefined')
				{
					$.bindRequiredInputFields();
				}
				else if(formsubmit == true)
				{
					formautosubmit = true;
					if(typeof inputelement !== 'undefined')
					{
						email_element = inputelement;
					}
					$.bindRequiredInputFields();
				}
				else if(formsubmit == false)
				{
					if(typeof inputelement !== 'undefined')
					{
						email_element = inputelement;
					}
					$.bindRequiredInputFields();
				}
			
			},	  
			bindRequiredInputFields : function()
			{
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $( window ).width() <= 480 ) {
					is_mobile = true;
					tooltip_class = 'tooltip_mobile';
					tooltip_underprocess_class = 'xverify_tooltip_underprocess_mobile';
					tooltip_error_class = 'xverify_tooltip_error_mobile';
					tooltip_warning_class = 'xverify_tooltip_warning_mobile';
					
				}	
				initializeVariables();
				initializeTimeOuts();
				initalizeServicesURL();
				initalizeAffiliatesParameters();
				initalizeDomainnameParameters();
				bindAffilateInputFields();
				bindRequiredInputFieldsByIdOrName();
				bindRequiredInputFieldsByClass();
				includeFiles();
			},
			xVerifyService : function(options)
			{
				
				var userOption = options;
				var submitType = 'onChange';
				userInputOption = userOption;
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $( window ).width() <= 480 ) {
					is_mobile = true;
					tooltip_class = 'tooltip_mobile';
					tooltip_underprocess_class = 'xverify_tooltip_underprocess_mobile';
					tooltip_error_class = 'xverify_tooltip_error_mobile';
					tooltip_warning_class = 'xverify_tooltip_warning_mobile';
					
				}
				/******************************** Over ride fields ************************/ 
				if(typeof userOption !== 'undefined' && typeof userOption.submitType !== 'undefined')
				{
					submitType = userOption.submitType;
				}
				if(typeof userOption !== 'undefined' && typeof userOption.formButton !== 'undefined')
				{
					initalizeFormButton(userOption.formButton);
				}
				else if(typeof userOption !== 'undefined' && typeof userOption.formButtons !== 'undefined')
				{
					fieldData = userOption.formButtons;
					for(var index in fieldData){
						elmentName = findElmentInPage(fieldData[index]);
						if(elmentName != '')
						{
							initalizeFormButton(elmentName);
							break;
						}
					}
					
				}
				if(typeof userOption !== 'undefined' && typeof userOption.services !== 'undefined')
				{
					if(typeof userOption.services.email  !== 'undefined' )
					{
						field_name = 'email';
						
						if(typeof userOption.services.email.tooltip_position  !== 'undefined' )
								elements_field_email.tooltip_position = userOption.services.email.tooltip_position;
								
						if(typeof userOption.services.email.field  !== 'undefined' )
						{
							
							elements_field_email.id = userOption.services.email.field;
							elements_field_email.name = userOption.services.email.field;
							elements_field_email.class = userOption.services.email.field;
						}
						else if(typeof userOption.services.email.fields  !== 'undefined' )
						{
							fieldData = userOption.services.email.fields;
							for(var index in fieldData){
								elmentName = findElmentInPage(fieldData[index]);
								if(elmentName != '')
								{
									elements_field_email.id = elmentName;
									elements_field_email.name = elmentName;
									elements_field_email.class = elmentName;
									break
								}
							}
						}
						
						if(typeof userOption.services.email.esp  !== 'undefined' )
						{
							if(typeof userOption.services.email.esp.esp_name  !== 'undefined' )
								elements_field_esp_name = userOption.services.email.esp.esp_name;
								
							if(typeof userOption.services.email.esp.list_id  !== 'undefined' )
								elements_field_list_id = userOption.services.email.esp.list_id;
								
							if(typeof userOption.services.email.esp.contact  !== 'undefined' )
								elements_field_esp_contacts = userOption.services.email.esp.contact;		
						}
					}
					
					if(typeof userOption.services.phone  !== 'undefined' )
					{
						field_name = 'phone';
						if(typeof userOption.services.phone.tooltip_position  !== 'undefined' )
								elements_field_phone.tooltip_position = userOption.services.phone.tooltip_position;
								
						if(typeof userOption.services.phone.field  !== 'undefined' )
						{
							
							elements_field_phone.id = userOption.services.phone.field;
							elements_field_phone.name = userOption.services.phone.field;
							elements_field_phone.class = userOption.services.phone.field;
						}
						else if(typeof userOption.services.phone.fields  !== 'undefined' )
						{
							fieldData = userOption.services.phone.fields;
							for(var index in fieldData){
								elmentName = findElmentInPage(fieldData[index]);
								if(elmentName != '')
								{
									elements_field_phone.id = elmentName;
									elements_field_phone.name = elmentName;
									elements_field_phone.class = elmentName;
									break
								}
							}
						}
					}
					
					if(typeof userOption.services.address  !== 'undefined' )
					{
						if(typeof userOption.services.address.tooltip_position  !== 'undefined' )
							{
								elements_field_street.tooltip_position = userOption.services.address.tooltip_position;
								elements_field_zip.tooltip_position = userOption.services.address.tooltip_position;
							}
							
						if(typeof userOption.services.address.fields  !== 'undefined' )
						{
							fieldData = userOption.services.address.fields;
							for(var index in fieldData){
								field_array = fieldData[index];
								street_field_find = false;
								street_2_field_find = false;
								zip_field_find = false;
								streetElmentName = '';
								zipElmentName = '';
								street2ElmentName = '';
								if(typeof field_array.street  !== 'undefined' )
								{
									streetElmentName = findElmentInPage(field_array.street);
									if(streetElmentName != '')
									{
										street_field_find = true;
									}
								}
								
								if(typeof field_array.zip  !== 'undefined' )
								{
									zipElmentName = findElmentInPage(field_array.zip);
									if(zipElmentName != '')
									{
										zip_field_find = true;
									}
								}
								
								if(typeof field_array.street_2  !== 'undefined' )
								{
									street2ElmentName = findElmentInPage(field_array.street_2);
									if(street2ElmentName != '')
									{
										street_2_field_find = true;
									}
								}
								
								if(street_field_find == true && zip_field_find == true)
								{
									elements_field_street.id = streetElmentName;
									elements_field_street.name = streetElmentName;
									elements_field_street.class = streetElmentName;
									
									elements_field_zip.id = zipElmentName;
									elements_field_zip.name = zipElmentName;
									elements_field_zip.class = zipElmentName;
									
									if(street_2_field_find == true)
									{
										elements_field_street_2.id = street2ElmentName;
										elements_field_street_2.name = street2ElmentName;
										elements_field_street_2.class = street2ElmentName;
									}
									break;
								}	
							}
						}
						else
						{
							if(typeof userOption.services.address.street_field  !== 'undefined' )
							{
								
								elements_field_street.id = userOption.services.address.street_field;
								elements_field_street.name = userOption.services.address.street_field;
								elements_field_street.class = userOption.services.address.street_field;
							}
							
							if(typeof userOption.services.address.zip_field  !== 'undefined' )
							{
								
								elements_field_zip.id = userOption.services.address.zip_field;
								elements_field_zip.name = userOption.services.address.zip_field;
								elements_field_zip.class = userOption.services.address.zip_field;
							}
							if(typeof userOption.services.address.street_field_2  !== 'undefined' )
							{
								
								elements_field_street_2.id = userOption.services.address.street_field_2;
								elements_field_street_2.name = userOption.services.address.street_field_2;
								elements_field_street_2.class = userOption.services.address.street_field_2;
							}
						}
					}
					
					
				}	
				
				initializeVariables();
				initializeTimeOuts();
				initalizeServicesURL();
				initalizeAffiliatesParameters();
				initalizeDomainnameParameters();
				
				bindAffilateInputFields();
				includeFiles();
				if(submitType == 'onSubmit')
				{
					globalSubmitType = submitType;
					$("form").each(function() {
						onsubmit_event = $(this).attr('onsubmit');
						if(typeof onsubmit_event  === 'undefined' )
						{
						}
						else
						{
							onsubmitbind = true;
							onsubmitstring = onsubmit_event;
							$(this).removeAttr('onsubmit');
							$(this).attr('form_count',form_count);
							var form_data = []
							form_data.fun_status = onsubmitbind;
							form_data.fun_name = onsubmitstring;
							forms[form_count] = form_data;
							form_count++;
						}
						$(this).xvBindFirst('submit',xvFormSubmitHandler);
					});
				}
				else
				{
					bindRequiredInputFieldsByIdOrName();
					bindRequiredInputFieldsByClass();
				}
				
			},
			validateEmailOnSubmit : function(inputelement,formname)
			{
				elements_field_email.id = inputelement;
				elements_field_email.name = inputelement;
				elements_field_email.class = inputelement;
				if(typeof formname  === 'undefined' )
				{
					formname = $('#'+inputelement).parents("form")[0];
				}
  				verify_email_submit = false;
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $( window ).width() <= 480 ) {
					is_mobile = true;
					tooltip_class = 'tooltip_mobile';
					tooltip_underprocess_class = 'xverify_tooltip_underprocess_mobile';
					tooltip_error_class = 'xverify_tooltip_error_mobile';
					tooltip_warning_class = 'xverify_tooltip_warning_mobile';
				}	
				initializeVariables();
				initializeTimeOuts();
				initalizeServicesURL();
				initalizeAffiliatesParameters();
				initalizeDomainnameParameters();
				bindAffilateInputFields();
				bindFields = bindUserFieldsByIdOrNameOrClass('email',formname);
				if(bindFields !== false)
				{
					service_status_bind = bindFields.attr('service');
					if(service_status_bind == 0 || service_status_bind == '0')
					{
						bindFields.unbind('change', emailChangeHandler);
						bindFields.unbind('xv_submit_form', emailChangeHandler);
						return;
					}
					
					bindFields.trigger('xv_submit_form');
					bindFields.unbind('change', emailChangeHandler);
					bindFields.unbind('xv_submit_form', emailChangeHandler);
					
					setTimeout(function(){
						checkValidateTimeout(formname,'email');
					},1000);
				}
				return verify_email_submit;
			},
			
			validatePhoneOnSubmit : function(inputelement,formname)
			{
				elements_field_phone.id = inputelement;
				elements_field_phone.name = inputelement;
				elements_field_phone.class = inputelement;
				if(typeof formname  === 'undefined' )
				{
					formname = $('#'+inputelement).parents("form")[0];
				}
				verify_phone_submit = false;
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $( window ).width() <= 480 ) {
					is_mobile = true;
					tooltip_class = 'tooltip_mobile';
					tooltip_underprocess_class = 'xverify_tooltip_underprocess_mobile';
					tooltip_error_class = 'xverify_tooltip_error_mobile';
					tooltip_warning_class = 'xverify_tooltip_warning_mobile';
					
				}	
				initializeVariables();
				initializeTimeOuts();
				initalizeServicesURL();
				initalizeAffiliatesParameters();
				initalizeDomainnameParameters();
				bindAffilateInputFields();
				bindFields = bindUserFieldsByIdOrNameOrClass('phone',formname);
				if(bindFields !== false)
				{
					service_status_bind = bindFields.attr('service');
					if(service_status_bind == 0 || service_status_bind == '0')
					{
						bindFields.unbind('change', phoneChangeHandler);
						bindFields.unbind('change', multiPhoneOtherFieldChangeHandler);
						bindFields.unbind('change', multiPhoneChangeHandler);
						return;
					}
					bindFields.trigger('change');
					bindFields.unbind('change', phoneChangeHandler);
					bindFields.unbind('change', multiPhoneOtherFieldChangeHandler);
					bindFields.unbind('change', multiPhoneChangeHandler);
					setTimeout(function(){
						checkValidateTimeout(formname,'phone');
						checkValidateTimeout(formname,'xverify_multifield_phone_3');
					},1000);
				}
				return verify_phone_submit;
			},
			
			validateAddressOnSubmit : function(inputelement,formname)
			{
				elements_field_street.id = inputelement;
				elements_field_street.name = inputelement;
				elements_field_street.class = inputelement;
				if(typeof formname  === 'undefined' )
				{
					formname = $('#'+inputelement).parents("form")[0];
				}
  				verify_address_submit = false;
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $( window ).width() <= 480 ) {
					is_mobile = true;
					tooltip_class = 'tooltip_mobile';
					tooltip_underprocess_class = 'xverify_tooltip_underprocess_mobile';
					tooltip_error_class = 'xverify_tooltip_error_mobile';
					tooltip_warning_class = 'xverify_tooltip_warning_mobile';
					
				}	
				initializeVariables();
				initializeTimeOuts();
				initalizeServicesURL();
				initalizeAffiliatesParameters();
				initalizeDomainnameParameters();
				bindAffilateInputFields();
				bindFields = bindUserFieldsByIdOrNameOrClass('address',formname);
				if(bindFields !== false)
				{
					trigger_data = bindFields.trigger_data;
					bind_data = bindFields.bind_data;
					service_status_bind = trigger_data.attr('service');
					if(service_status_bind == 0 || service_status_bind == '0')
					{
						bind_data.unbind('change', streetChangeHandler);
						bind_data.unbind('change', addressOtherFieldChangeHandler);
						return;
					}
					trigger_data.trigger('change');
					bind_data.unbind('change', streetChangeHandler);
					bind_data.unbind('change', addressOtherFieldChangeHandler);
					setTimeout(function(){
						checkValidateTimeout(formname,'address');
					},1000);
				}
				return verify_address_submit;
			},
			bindXVServicesOnInputFields : function(formname)
			{
				formautosubmit = true;
				fields = $(formname).find('input');
				$(fields).each(
					function(index){  
						var input = $(this);
					
	if(input.attr('name') == elements_field_email.name || input.attr('id') == elements_field_email.name || input.hasClass(elements_field_email.name))
	{
		$.validateEmailOnSubmit(elements_field_email.name,formname);
	}
	else if(input.attr('name') == elements_field_email.id || input.attr('id') == elements_field_email.id || input.hasClass(elements_field_email.id))
	{
		$.validateEmailOnSubmit(elements_field_email.id,formname);
	}
	else if(input.attr('name') == elements_field_email.class || input.attr('id') == elements_field_email.class || input.hasClass(elements_field_email.class))
	{
		$.validateEmailOnSubmit(elements_field_email.class,formname);
	}
	else if(input.attr('name') == elements_field_phone.name || input.attr('id') == elements_field_phone.name || input.hasClass(elements_field_phone.name))
	{
		$.validatePhoneOnSubmit(elements_field_phone.name,formname);
	}
	else if(input.attr('name') == elements_field_phone.id || input.attr('id') == elements_field_phone.id || input.hasClass(elements_field_phone.id))
	{
		$.validatePhoneOnSubmit(elements_field_phone.name,formname);
	}
	else if(input.attr('name') == elements_field_phone.class || input.attr('id') == elements_field_phone.class || input.hasClass(elements_field_phone.class))
	{
		$.validatePhoneOnSubmit(elements_field_phone.class,formname);
	}
	else if(input.hasClass('xverify_multifield_phone_1'))
	{
		$.validatePhoneOnSubmit(elements_field_phone.class,formname);
	}
	else if(input.attr('name') == elements_field_street.name || input.attr('id') == elements_field_street.name || input.hasClass(elements_field_street.name))
	{
		$.validateAddressOnSubmit(elements_field_street.name,formname);
	}
	else if(input.attr('name') == elements_field_street.id || input.attr('id') == elements_field_street.id || input.hasClass(elements_field_street.id))
	{
		$.validateAddressOnSubmit(elements_field_street.name,formname);
	}
	else if(input.attr('name') == elements_field_street.class || input.attr('id') == elements_field_street.class || input.hasClass(elements_field_street.class))
	{
		$.validateAddressOnSubmit(elements_field_street.class,formname);
	}
		
		
					}
				);
			}
			
		});
})(jQuery);	 
