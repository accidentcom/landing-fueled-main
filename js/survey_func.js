	/*  Wizard */
	jQuery(function ($) {
		"use strict";

		$(".read_more_terms").on('click', function(){
			$('.read-more-text').toggle();
		});

		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};
		
		$('#accident_auto_wizardv2').validate({
				rules: {
					address:{
						required: true
					}
				},
				errorPlacement: function (error, element) {
					if (element.is('select:hidden')) {
						error.insertAfter(element.next('.nice-select'));
					} else {
						error.insertAfter(element);
					}
				},
				messages: {
					address:{
						required: "Please Enter 5 Digit Zip Code"
					}
				}
		});

		$(".branch").addClass('hide_ele');
		$('.forward').text('Start Evaluation').css('width','100%');
		$("#wizard_container").wizard({
			transitions: {
				accident_type: function( state, action ) {
					var branch = $('input[name=accident_type]:checked').val();

					if ( !branch ) {
						alert( "Please select a value to continue." );
					}
					if($("[id='"+branch+"']").hasClass('hide_ele')){
						$("[id='"+branch+"']").removeClass('hide_ele');
						$(".branch:not([id='"+branch+"'])").addClass('hide_ele');
					}else{
						$(".branch:not([id='"+branch+"'])").addClass('hide_ele');
					}
					$(".branch:not([id='"+branch+"']) input.error").removeClass('error');
					$(".branch:not([id='"+branch+"']) span.error").remove();

					$(".branch:not([id='"+branch+"']) input[type='radio']:not([name='accident_type'])").prop('checked', false);
					$(".branch:not([id='"+branch+"']) input[type='text']:not([name='address']), textarea ").val('');

					return branch;
				}
			},
			stepsWrapper: "#accident_auto_wizard",
			submit: ".submit",
			beforeSelect: function (event, state) {
				if ($('input#website').val().length != 0) {
					return false;
				}
				if (!state.isMovingForward)
					return true;

				var lT = 0;
	
				$(".address-info > input").each(function() {
					lT += $(this).val().length;
				});
				if(lT == 0){
				$("#address").val('');
				var inputs = $(this).wizard('state').step.find(':input');
				return !inputs.length || !!inputs.valid();
				}else{
					var inputs = $(this).wizard('state').step.find(':input');
					return !inputs.length || !!inputs.valid();
				}				
			}
		}).validate({
			errorPlacement: function (error, element) {
				if (element.is(':radio') || element.is(':checkbox')) {
					error.insertBefore(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
		//  progress bar
		$("#progressbar").progressbar();
		var temp_branches;
		$("#wizard_container").wizard({
			afterSelect: function (event, state) {
				$("#progressbar").progressbar("value", state.percentComplete);
				$("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
			},
			afterForward: function(event, state){
				if(state.isLastStepInBranch){
					var branch = state.branch[0].id;
					$(".branch:not([id='"+branch+"']) input.error").removeClass('error');
					$(".branch:not([id='"+branch+"']) span.error").remove();
					$(".branch:not([id='"+branch+"']) div.step").addClass('hide_ele');
					temp_branches = $(".branch:not([id='"+branch+"'])").detach();
				}

				if(isMobile.any()){
					$('main, footer').addClass('d-none');
				}
				
				$('.forward').text('Next').css('width','50%');
				
			},
			afterBackward: function(event, state){
				if(!state.isLastStepInBranch){
					var branch = state.branch[0].id;
					if(temp_branches !== undefined){
						$("[id='"+branch+"']").after(temp_branches);
					}
				}
				if(state.stepIndex == 0){
					$('.forward').text('Start Evaluation').css('width','100%');
				}
			}
		});
		
	
	/* 	jQuery.validator.addMethod("validDate", function(value, element) {
			return this.optional(element) || moment(value,"DD/MM/YYYY").isValid();
		}, "Please  enter a valid date in the format DD/MM/YYYY");*/

		jQuery.validator.addMethod("validDate", function(value, element) {
			return isDate(value);
		}, "Please enter a valid date in the format MM/DD/YYYY");

		
		jQuery.validator.setDefaults({
			ignore: ".hide_ele" 
		});

		// Validate select
		$('#accident_auto_wizard').validate({
		/* 	ignore: ".hide_ele", */
			rules: {
				select: {
					required: true
				},
				address:{
					required: true
				},
				accident_incident_year: {
					required:true/* ,
					validDate:true */
				},
				description:{
					required:true
				},
				terms:{
					required:true
				}
			},
			errorPlacement: function (error, element) {
				if (element.is('select:hidden')) {
					error.insertAfter(element.next('.nice-select'));
				} else {
					error.insertAfter(element);
				}
			},
			messages: {
				address:{
					required: "Please Enter 5 Digit Zip Code"
				},
				description:{
					required:'Describe Your Case'
				}
			},
			submitHandler: function(form,event) {
				event.preventDefault();
				var form = $("form#accident_auto_wizard");
				var data = $(form).serializeArray();
				form.validate();
				if (form.valid()) {
					$("#loader_form").fadeIn();
					save_lead(data);
				}
			}
		});

		//auto
		$("input[name$='[auto]']").each(function () {
			$(this).rules("add", {
				required: true
			});
		});

		$("input[name$='[medical]']").each(function () {
			$(this).rules("add", {
				required: true
			});
		});
	});

	// Summary 
	function getVals(formControl, controlType) {
		if($("#accident_type").text() == ''){
			$(".answer_accident_type").text($('input[name=accident_type]:checked').val());
		}
		var control = []
		var type = '';
		if(controlType.indexOf('[') != -1){
			control = controlType.split('[');
			controlType = control[0];
			type = "["+control[1];
		}

		switch (controlType) {

			case 'date':
				// Get the value for a radio
				var value = $(formControl).val();
				$(".answer_date").text(value);
				break;

			case 'accident_type':
				// Get the value for a radio
				var value = $(formControl).val();
				$(".answer_accident_type").text(value);
				break;

			case 'got_attorney':
				// Get the value for a radio
				var value = $(formControl).val();
				if(type != ''){
					var summary_answer = "answer_got_attorney"+type;
					$('[class="'+summary_answer+'"]').text(value);
				}else{
					$(".answer_got_attorney").text(value);
				}
				break;

			case 'missed_work':
			// Get the value for a radio
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_missed_work"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_missed_work").text(value);
			}
			break;

			case 'medical_claim':
			// Get the value for a radio
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_medical_claim"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_medical_claim").text(value);
			}
			break;
			
			case 'primary_injury':
				// Get the value for a radio
				//var value = $(formControl).val();
					// Get name for set of checkboxes
				var checkboxName = $(formControl).attr('name');

				// Get all checked checkboxes
				var value = [];
				$("input[name*='" + checkboxName + "']").each(function () {
					// Get all checked checboxes in an array
					if (jQuery(this).is(":checked")) {
						value.push($(this).val());
					}
				});

				if(type != ''){
					var summary_answer = "answer_primary_injury"+type;
					$('[class="'+summary_answer+'"]').text(value);
				}else{
					$(".answer_primary_injury").text(value.join(", "));
				}

				break;

			case 'medical_injury':
			// Get the value for a radio
			//var value = $(formControl).val();
				// Get name for set of checkboxes
			var checkboxName = $(formControl).attr('name');

			// Get all checked checkboxes
			var value = [];
			$("input[name*='" + checkboxName + "']").each(function () {
				// Get all checked checboxes in an array
				if (jQuery(this).is(":checked")) {
					value.push($(this).val());
				}
			});

			if(type != ''){
				var summary_answer = "answer_medical_injury"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_medical_injury").text(value.join(", "));
			}

			break;

			case 'primary_injury_type':
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_primary_injury_type"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_primary_injury_type").text(value);
			}

			break;
			
			case 'medical_treatment':
				// Get the value for a radio
				var value = $(formControl).val();
				if(type != ''){
					var summary_answer = "answer_medical_treatment"+type;
					$('[class="'+summary_answer+'"]').text(value);
				}else{
					$(".answer_medical_treatment").text(value);
				}
				break;

			case 'at_fault':
				// Get the value for a radio
				var value = $(formControl).val();
				if(type != ''){
					var summary_answer = "answer_at_fault"+type;
					$('[class="'+summary_answer+'"]').text(value);
				}else{
					$(".answer_at_fault").text(value);
				}
				break;
			case 'vehicle_type':
			// Get the value for a checkbox
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_vehicle_type"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_vehicle_type").text(value);
			}
			break;
				
			case 'description':
				// Get the value for a textarea
				var value = $(formControl).val();
				$(".answer_description").text(value);
				break;

			case 'main_injury_type':
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_main_injury_type"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_main_injury_type").text(value);
			}
			break;

			case 'relationship':
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_relationship"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_relationship").text(value);
			}
			
			break;

			case 'victim_death_cause':
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_victim_death_cause"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_victim_death_cause").text(value);
			}
			break;

			case 'filed_case':
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_filed_case"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_filed_case").text(value);
			}
			break;
			case 'while_at_work':
			var value = $(formControl).val();
			if(type != ''){
				var summary_answer = "answer_while_at_work"+type;
				$('[class="'+summary_answer+'"]').text(value);
			}else{
				$(".answer_while_at_work").text(value);
			}
			break;
			
				//==========================old questionssssssssssss demo theme's below=========================================
			case 'question_1':
				// Get the value for a radio
				var value = $(formControl).val();
				$("#question_1").text(value);
				break;

			case 'question_2':
				// Get name for set of checkboxes
				var checkboxName = $(formControl).attr('name');

				// Get all checked checkboxes
				var value = [];
				$("input[name*='" + checkboxName + "']").each(function () {
					// Get all checked checboxes in an array
					if (jQuery(this).is(":checked")) {
						value.push($(this).val());
					}
				});
				$("#question_2").text(value.join(", "));
				break;

			case 'question_3':
				// Get the value for a radio
				var value = $(formControl).val();
				$("#question_3").text(value);
				break;

			case 'additional_message':
				// Get the value for a textarea
				var value = $(formControl).val();
				$("#additional_message").text(value);
				break;
		}
	}

	//save lead to db
function save_lead(data){
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        method: "POST", // Type of response and matches what we said in the route
        url: "save-lead", // This is the url we gave in the route
        data: data, // a JSON object to send back
        success: function (response) {
            response = JSON.parse(response);
            if (response && response.status == 'success') {
                window.location = response.redirect_to;
            } else {
                alert('Failed');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

            console.log(
                "AJAX error:"
            );
        }
    });
}

function isDate(txtDate) {
	var currVal = txtDate;
	if (currVal == '') {
		return false;
	}
	//Declare Regex  
	var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
	var dtArray = currVal.match(rxDatePattern); // is format OK?

	if (dtArray == null) {
		return false;
	}
	//check which date-format is being used

	/*
	if (date == dd/mm/yyyy) {
		//Checks for dd/mm/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];
	} else if (date == mm/dd/yyyy) {
		//Checks for mm/dd/yyyy format.
		dtMonth = dtArray[1];
		dtDay= dtArray[3];
		dtYear = dtArray[5];
	} else if (date == yyyy/mm/dd) {
		//Checks for yyyy/mm/dd
		dtYear = dtArray[1];
		dtMonth = dtArray[3];
		dtDay = dtArray[5];
	}
	*/

	dtDay = dtArray[3];
	dtMonth = dtArray[1];
	dtYear = dtArray[5];

	if (dtMonth < 1 || dtMonth > 12) {
		return false;
	} else if (dtDay < 1 || dtDay > 31) {
		return false;
	} else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
		return false;
	} else if (dtMonth == 2) {
		var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	}
	if (dtDay > 29 || (dtDay == 29 && !isleap)) {
		return false;
	}
	return true;
}
